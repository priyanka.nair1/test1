﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" MadCap:onlyLocalStylesheets="False" class="task">
    <head>
        <link href="../../Resources/TableStyles/Default-Table.css" rel="stylesheet" MadCap:stylesheetType="table" /><title>Creating a Connection</title>
        <link href="../../Resources/Stylesheets/Styles.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <h1><a name="top"></a>Creating a Connection</h1>
        <p>Connection refers to the configuration setup for connecting IGA to target applications. You create connections in IGA by specifying the connection parameters. Because connection parameters are different for different connection types, connections are the subset of a connection type. This means that a single connection type can have one or multiple&#160;connections.</p>
        <div class="note" MadCap:autonum="&lt;b&gt;Note&lt;/b&gt;">
            <ul>
                <li>
                    <p class="note">Due to the duplicate connection type in the Connection list page, the code 'sav.connectionTypeListAsCsv' has been blocked from the <label class="filename-path">externalconfig.properties</label>&#160;file.</p>
                </li>
                <li>
                    <p class="note">IGA supports a number of predefined connectors that are specifically designed for target applications. To integrate IGA with a target application that has no corresponding predefined connector, you create a connection for the connector.</p>
                    <p class="note" MadCap:conditions="Default.v2020X">&#160;For a detailed list of supported connectors, see the <MadCap:conditionalText MadCap:conditions="Default.Link"><a href="Saviynt-Enterprise-Identity-Cloud-Connectors.htm" pubname="Connectors-v2020x" class="peer">Saviynt Identity Governance and Administration Cloud Connectors</a></MadCap:conditionalText>.</p>
                    <p class="note" MadCap:conditions="Default.v2021X">&#160;For a detailed list of supported connectors, see the <MadCap:conditionalText MadCap:conditions="Default.Link"><a href="Saviynt-Enterprise-Identity-Cloud-Connectors.htm" pubname="Connectors-v2021x" class="peer">Saviynt Identity Governance and Administration Cloud Connectors</a></MadCap:conditionalText>.</p>
                </li>
            </ul>
        </div>
        <p>Perform the following steps to create a connection:</p>
        <ol>
            <li>
                <p>Log in to IGA.</p>
            </li>
            <li>
                <p>Go to <strong>ADMIN &gt; Identity Repository &gt; Connections</strong>.</p>
            </li>
            <li>
                <p>Click <strong>Action</strong> &gt; <strong>Create Connection</strong>.</p>
            </li>
            <li>
                <p>In the <strong>Add/Update Connection</strong> page that displays, specify the connection parameters. For more information about the most common connection parameters, see the <em>Understanding the Connection Parameters</em> section.&#160;</p>
                <p class="note" MadCap:conditions="Default.v2020X">For more information about specific connectors, see the <MadCap:conditionalText MadCap:conditions="Default.Link"><a href="Saviynt-Enterprise-Identity-Cloud-Connectors.htm" pubname="Connectors-v2020x" class="peer">Saviynt Identity Governance and Administration Cloud Connectors</a></MadCap:conditionalText>.</p>
                <p class="note" MadCap:conditions="Default.v2021X">For more information about specific connectors, see the <MadCap:conditionalText MadCap:conditions="Default.Link"><a href="Saviynt-Enterprise-Identity-Cloud-Connectors.htm" pubname="Connectors-v2021x" class="peer">Saviynt Identity Governance and Administration Cloud Connectors</a></MadCap:conditionalText>.</p>
            </li>
            <li>
                <p>Click <strong>Save</strong> or&#160;<strong>Save and Test Connection </strong>to save the connection.</p>
                <ul>
                    <li>
                        <p>The <strong>Save</strong> button is displayed for these connection types: Workday-SOAP , Workday-REST , WebService , Slack , SOAP , SalesForce , RPA , RACF , PeopleSoft , OIM , JarConnector , GithubRest , Github , GCP , File , EPIC , Duo , Citrix, and Chef.&#160; The saved connection values are stored in the database. &#160;</p>
                    </li>
                    <li>
                        <p>The <b>Save and Test Connection</b> button is displayed for these connection types:&#160;SAP, HANA , GoogleApps , Office365 , ORACLE-EBS , REST , AzureAD , Azure , WINCONNECTOR , Unix , Box , Okta , AWS , DB , and AD. The saved connection values are stored in the database and the connection is tested to verify if it is successful. The status of the test is displayed in a message.&#160;</p>
                    </li>
                </ul>
            </li>
        </ol>
        <p>For a REST connection type, you can populate the values all connection parameters by clicking the&#160;<strong>Populate Connection JSON</strong> icon adjacent to the labels of connection parameters. To use this icon, enable <strong style="text-align: left;">Rest Questionnaire</strong>&#160;under <strong style="text-align: left;">Preferences</strong> in&#160;Global Configurations. </p>
        <p MadCap:conditions="Default.v2020X">For more information about the attributes used for defining connection parameters, see <MadCap:conditionalText MadCap:conditions="Default.Link"><a href="Developers-Handbook.htm" pubname="REST-v2020x" class="peer">Developers Handbook</a></MadCap:conditionalText> in the <em>REST Connector Guide</em>.</p>
        <p MadCap:conditions="Default.v2021X">For more information about the attributes used for defining connection parameters, see <MadCap:conditionalText MadCap:conditions="Default.Link"><a href="Developers-Handbook.htm" pubname="REST-v2021x" class="peer">Developers Handbook</a></MadCap:conditionalText> in the <em>REST Connector Guide</em>.</p>
        <h2 id="CreatingaConnection-UnderstandingtheConnectionParameters"><a name="Understa"></a>Understanding the Connection Parameters</h2>
        <p>The connection parameters are displayed under&#160;two categories&#160;<b>Basic Config</b> and <strong style="color: rgb(0,0,0);background-color: rgb(255,255,255);">Advanced Config</strong>. The <strong style="color: rgb(0,0,0);background-color: rgb(255,255,255);">Basic Config</strong> category displays the minimum set of parameters required to establish a connection and the&#160;<strong style="color: rgb(0,0,0);background-color: rgb(255,255,255);">Advanced Config</strong> category displays the advanced parameters. When you define and save the values for the parameters in <b>Basic Config</b>, those values are automatically populated in the <strong>Advanced Config</strong> page for the parameters where they are referred.&#160;To modify the values for advanced parameters, click <b>Advanced Config.</b></p>
        <p>Connection parameters vary for each connection. These parameters can be broadly categorized into connection, import, and provisioning parameters to help you populate the values for the parameters required for a specific connection type.&#160;For example,&#160;CREATEACCESSJON, GRANTACCESSJSON, and REVOKEACCESSJSON parameters are used for&#160;performing provisioning&#160;operations on the target application. </p>
        <p class="note" MadCap:conditions="Default.v2020X">Before creating a connection, you must review the connection parameters required for your connection from the <MadCap:conditionalText MadCap:conditions="Default.Link"><a href="Saviynt-Enterprise-Identity-Cloud-Connectors.htm" pubname="Connectors-v2020x" class="peer">Saviynt Identity Governance and Administration Cloud Connectors</a></MadCap:conditionalText>. If you do not find a dedicated&#160;connector guide&#160;listed for your application, raise a Freshdesk ticket to contact the Saviynt support team for further assistance.</p>
        <p class="note" MadCap:conditions="Default.v2021X">Before creating a connection, you must review the connection parameters required for your connection from the <MadCap:conditionalText MadCap:conditions="Default.Link"><a href="Saviynt-Enterprise-Identity-Cloud-Connectors.htm" pubname="Connectors-v2021x" class="peer">Saviynt Identity Governance and Administration Cloud Connectors</a></MadCap:conditionalText>. If you do not find a dedicated&#160;connector guide&#160;listed for your application, raise a Freshdesk ticket to contact the Saviynt support team for further assistance.</p>
        <p>The following table describes the most of the common parameters specified for establishing a connection with a target system.</p>
        <p>Note that your connection might use some or all of these parameters. Some connectors might include additional parameters.</p>
        <table class="TableStyle-Default-Table" style="width: 100%;mc-table-style: url('../../Resources/TableStyles/Default-Table.css');" cellspacing="0">
            <col class="TableStyle-Default-Table-Column-Column1" />
            <col class="TableStyle-Default-Table-Column-Column1" />
            <thead>
                <tr class="TableStyle-Default-Table-Head-Header1">
                    <th class="TableStyle-Default-Table-HeadE-Column1-Header1">Parameter</th>
                    <th class="TableStyle-Default-Table-HeadD-Column1-Header1">Description</th>
                </tr>
            </thead>
            <tbody>
                <tr class="TableStyle-Default-Table-Body-Body1">
                    <td class="TableStyle-Default-Table-BodyE-Column1-Body1">
                        <p class="table-text">Connection Name</p>
                    </td>
                    <td class="TableStyle-Default-Table-BodyD-Column1-Body1">
                        <p class="table-text">Specify the name to identify the connection.</p>
                    </td>
                </tr>
                <tr class="TableStyle-Default-Table-Body-Body1">
                    <td class="TableStyle-Default-Table-BodyE-Column1-Body1">
                        <p class="table-text">Connection Description</p>
                    </td>
                    <td class="TableStyle-Default-Table-BodyD-Column1-Body1">
                        <p class="table-text">Specify the description for the connection.</p>
                    </td>
                </tr>
                <tr class="TableStyle-Default-Table-Body-Body1">
                    <td class="TableStyle-Default-Table-BodyE-Column1-Body1">
                        <p class="table-text">Connection Type</p>
                    </td>
                    <td class="TableStyle-Default-Table-BodyD-Column1-Body1">
                        <p class="table-text">Select a connection type or use the connection template.&#160;<br /></p>
                        <p class="table-text">For example, if your target application is Azure AD,&#160;specify connection type as <strong>Azure AD</strong>.</p>
                    </td>
                </tr>
                <tr class="TableStyle-Default-Table-Body-Body1">
                    <td class="TableStyle-Default-Table-BodyE-Column1-Body1">
                        <p class="table-text">Default SAV Role</p>
                    </td>
                    <td class="TableStyle-Default-Table-BodyD-Column1-Body1">
                        <p style="text-align: left;" class="table-text">Specify the SAV role to assign for the connection.</p>
                        <p style="text-align: left;" class="table-text">The SAV role is a Saviynt role that assigns specific access to users.<br />For example, if a user is assigned the<strong> ROLE_ADMIN</strong> role, the user has access to all the sections of IGA.</p>
                        <p style="text-align: left;" class="table-text">This parameter is valid only for importing users.</p>
                    </td>
                </tr>
                <tr class="TableStyle-Default-Table-Body-Body1">
                    <td class="TableStyle-Default-Table-BodyE-Column1-Body1">
                        <p class="table-text">Email Template</p>
                    </td>
                    <td class="TableStyle-Default-Table-BodyD-Column1-Body1">
                        <p style="text-align: left;" class="table-text">Specify the email template applicable for notifications.</p>
                        <p style="text-align: left;" class="table-text">Email notifications provide immediate triggers of emails to a respective user based on actions performed.</p>
                        <p style="text-align: left;" class="table-text">Email acts as one of the notifications to the user informing which action has been performed and if critical, needs immediate action from the user.</p>
                    </td>
                </tr>
                <tr class="TableStyle-Default-Table-Body-Body1">
                    <td colspan="1" class="TableStyle-Default-Table-BodyE-Column1-Body1">
                        <p class="table-text">SSL Certificate</p>
                    </td>
                    <td colspan="1" class="TableStyle-Default-Table-BodyD-Column1-Body1">
                        <p class="table-text">Specify the SSL certificates to use for securing the connection between IGA and the target application to encrypt the data shared between them.&#160;</p>
                        <p class="table-text">From Release v5.5.2 SP2 onwards, you can use the user interface to map the SSL certificate with the connection.&#160;The certificates are stored in the IGA trust store.&#160;This was done outside of IGA in the previous releases. For detailed information, see&#160;<MadCap:xref href="../../Chapter07-General-Administrator-Tasks/Certificate-Management.htm">Certificate Management.</MadCap:xref></p>
                        <p style="text-align: left;" class="table-text">Specify the SSL certificates to use for securing the connection between IGA and the target application to encrypt the data shared between them.</p>
                        <p style="text-align: left;" class="table-text">From Release v5.5.2 SP2 onwards, you can use the user interface to map the SSL certificate with the connection. The certificates are stored in the IGA trust store. This was done outside of IGA in the previous releases.&#160;For detailed information, see <MadCap:xref href="../../Chapter07-General-Administrator-Tasks/Certificate-Management.htm">Certificate Management</MadCap:xref>.</p>
                        <p style="text-align: left;" class="table-text">Use the&#160;<strong>Add Certificate</strong>button to&#160;add and map new SSL certificates to the IGA trust store. IGA provides multiple options to add new SSL certificates.</p>
                        <p style="text-align: left;" class="table-text">Use the&#160;<strong>Overwrite Existing</strong>button to overwrite an existing SSL certificate.</p>
                        <p style="text-align: left;" class="table-text">For detailed information, see&#160;<MadCap:xref href="#Understa">Understanding the Connection Parameters</MadCap:xref>.&#160;</p>
                    </td>
                </tr>
                <tr class="TableStyle-Default-Table-Body-Body1">
                    <td colspan="1" class="TableStyle-Default-Table-BodyE-Column1-Body1">
                        <p class="table-text">Credential Vault Connection</p>
                    </td>
                    <td colspan="1" class="TableStyle-Default-Table-BodyD-Column1-Body1">
                        <p class="table-text">Select a credential vault from the drop-down list.</p>
                    </td>
                </tr>
                <tr class="TableStyle-Default-Table-Body-Body1">
                    <td colspan="1" class="TableStyle-Default-Table-BodyE-Column1-Body1">
                        <p class="table-text">Save Credential Vault</p>
                    </td>
                    <td colspan="1" class="TableStyle-Default-Table-BodyD-Column1-Body1">
                        <p class="table-text">Select this parameter to save the encrypted attribute in the vault configured with the connector.</p>
                    </td>
                </tr>
                <tr class="TableStyle-Default-Table-Body-Body1">
                    <td colspan="1" class="TableStyle-Default-Table-BodyE-Column1-Body1">
                        <p class="table-text">Vault Config</p>
                    </td>
                    <td colspan="1" class="TableStyle-Default-Table-BodyD-Column1-Body1">
                        <p class="table-text">Specify the path of the vault to obtain the secret data. Suffix the name of the connector to this path to make it unique for the connector.</p>
                        <p class="table-text">After the path is updated, click <strong>Advanced</strong> to map the encrypted attributes of the connector with the vault secret key.</p>
                        <p class="table-text">To convert the key value from the base64 format to the text format while obtaining the key from the vault, enable <strong>Is Encoded</strong>.</p>
                    </td>
                </tr>
                <tr class="TableStyle-Default-Table-Body-Body1">
                    <td colspan="1" class="TableStyle-Default-Table-BodyE-Column1-Body1">
                        <p class="table-text">Hostname_IP</p>
                    </td>
                    <td colspan="1" class="TableStyle-Default-Table-BodyD-Column1-Body1">
                        <p class="table-text">Specify the hostname or the IP address of the machine where the target application is installed.</p>
                        <p class="table-text">For example, if your target application is Active Directory,&#160;specify the hostname as hostname.domain.com and&#160;IP address as 10.1.1.5.</p>
                    </td>
                </tr>
                <tr class="TableStyle-Default-Table-Body-Body1">
                    <td colspan="1" class="TableStyle-Default-Table-BodyE-Column1-Body1">
                        <p class="table-text">Port</p>
                    </td>
                    <td colspan="1" class="TableStyle-Default-Table-BodyD-Column1-Body1">
                        <p class="table-text">Specify the port number to use for connecting to the target application.&#160;</p>
                    </td>
                </tr>
                <tr class="TableStyle-Default-Table-Body-Body1">
                    <td class="TableStyle-Default-Table-BodyE-Column1-Body1">
                        <p class="table-text">Protocol</p>
                    </td>
                    <td class="TableStyle-Default-Table-BodyD-Column1-Body1">
                        <p class="table-text">Specify the protocol for connecting to the target application.</p>
                    </td>
                </tr>
                <tr class="TableStyle-Default-Table-Body-Body1">
                    <td class="TableStyle-Default-Table-BodyE-Column1-Body1">
                        <p class="table-text">Username</p>
                    </td>
                    <td class="TableStyle-Default-Table-BodyD-Column1-Body1">
                        <p class="table-text">Specify the username to connect to the target application. For example, if your target application is Active Directory, it refers to the username of an account or a service account that has the necessary permissions to access Active Directory.&#160;&#160;</p>
                    </td>
                </tr>
                <tr class="TableStyle-Default-Table-Body-Body1">
                    <td class="TableStyle-Default-Table-BodyE-Column1-Body1">
                        <p class="table-text">Password</p>
                    </td>
                    <td class="TableStyle-Default-Table-BodyD-Column1-Body1">
                        <p class="table-text">Specify the password  to connect to the target application.</p>
                    </td>
                </tr>
                <tr class="TableStyle-Default-Table-Body-Body1">
                    <td class="TableStyle-Default-Table-BodyE-Column1-Body1">
                        <p class="table-text">DRIVERNAME</p>
                    </td>
                    <td class="TableStyle-Default-Table-BodyD-Column1-Body1">Specify the driver name for the connection. For example, if your target application is&#160;Oracle EBS,&#160;specify&#160;driver name as&#160;oracle.jdbc.OracleDriver.&#160;</td>
                </tr>
                <tr class="TableStyle-Default-Table-Body-Body1">
                    <td class="TableStyle-Default-Table-BodyE-Column1-Body1">
                        <p class="table-text">PASSWORD_MIN_LENGTH<br /></p>
                    </td>
                    <td class="TableStyle-Default-Table-BodyD-Column1-Body1">
                        <p class="table-text">Specify the minimum length for the random password.</p>
                    </td>
                </tr>
                <tr class="TableStyle-Default-Table-Body-Body1">
                    <td class="TableStyle-Default-Table-BodyE-Column1-Body1">
                        <p class="table-text">PASSWORD_MAX_LENGTH</p>
                    </td>
                    <td class="TableStyle-Default-Table-BodyD-Column1-Body1">
                        <p class="table-text">Specify the maximum length for the random password.</p>
                    </td>
                </tr>
                <tr class="TableStyle-Default-Table-Body-Body1">
                    <td colspan="1" class="TableStyle-Default-Table-BodyE-Column1-Body1">
                        <p class="table-text">PASSWORD_NOOFCAPSALPHA<br /></p>
                    </td>
                    <td colspan="1" class="TableStyle-Default-Table-BodyD-Column1-Body1">
                        <p class="table-text">Specify the number of upper case alphabets required for the random password.</p>
                    </td>
                </tr>
                <tr class="TableStyle-Default-Table-Body-Body1">
                    <td colspan="1" class="TableStyle-Default-Table-BodyE-Column1-Body1">
                        <p class="table-text">PASSWORD_NOOFDIGITS</p>
                    </td>
                    <td colspan="1" class="TableStyle-Default-Table-BodyD-Column1-Body1">
                        <p class="table-text">Specify the number of digits required for the random password.</p>
                    </td>
                </tr>
                <tr class="TableStyle-Default-Table-Body-Body1">
                    <td colspan="1" class="TableStyle-Default-Table-BodyE-Column1-Body1">
                        <p class="table-text">PASSWORD_NOOFSPLCHARS</p>
                    </td>
                    <td colspan="1" class="TableStyle-Default-Table-BodyD-Column1-Body1">
                        <p class="table-text">Specify the number of special characters required for the random password.</p>
                    </td>
                </tr>
                <tr class="TableStyle-Default-Table-Body-Body1">
                    <td colspan="1" class="TableStyle-Default-Table-BodyE-Column1-Body1">
                        <p class="table-text">CREATEACCOUNTJSON</p>
                    </td>
                    <td colspan="1" class="TableStyle-Default-Table-BodyD-Column1-Body1">Specify this parameter to provision newly created accounts to the target application.&#160;Objects exposed are random Password, task, user, account Name, role, endpoint and all the objects defined in dynamic attributes. <p class="table-text">If your target application is&#160;Azure AD, specify this parameter in the following format:</p><MadCap:codeSnippet><MadCap:codeSnippetCopyButton aria-label="Copy" title="Copy to clipboard" /><MadCap:codeSnippetCaption>JSON</MadCap:codeSnippetCaption><MadCap:codeSnippetBody MadCap:useLineNumbers="False" MadCap:lineNumberStart="1" MadCap:continue="False" xml:space="preserve" style="padding-left: 12px;padding-right: 16px;padding-top: 12px;padding-bottom: 12px;mc-code-lang: SQL;">{
"accountIdPath":"call1.message.id",
"dateFormat":"yyyy-MM-dd'T'HH:mm:ssXXX",
"responseColsToPropsMap":{
"comments":"call1.message.displayName~#~char",
"displayName":"call1.message.displayName~#~char",
"name":"call1.message.userPrincipalName~#~char"
},
"call":[
{
"name":"call1",
"connection":"userAuth",
"url":"https://graph.microsoft.com/v1.0/users",
"httpMethod":"POST",
"httpParams":"{\"accountEnabled\":\"true\",\"displayName\":\"${user.displayname}\",\"passwordProfile\":\r\n{\"password\":\"Passw0rd\",\"forceChangePasswordNextSignIn\":\"true\"},\"UsageLocation\":\"US\",\"userPrincipalName\":\"${user.email}\",\"mailNickname\":\"${user.firstname}\",\"givenName\":\"${user.firstname}\",\"surname\":\"${user.lastname}\"}",
"httpHeaders":{
"Authorization":"${access_token}"
},
"httpContentType":"application/json",
"successResponses":{
"statusCode":[
200,
201,
204,
205
]
}
}
]
}</MadCap:codeSnippetBody></MadCap:codeSnippet><p class="table-text">&#160;</p></td>
                </tr>
                <tr class="TableStyle-Default-Table-Body-Body1">
                    <td colspan="1" class="TableStyle-Default-Table-BodyE-Column1-Body1">
                        <p class="table-text">GRANTACCESSJSON</p>
                    </td>
                    <td colspan="1" class="TableStyle-Default-Table-BodyD-Column1-Body1">
                        <p class="table-text">Specify the command to provision access to the target application.&#160;Objects exposed&#160; are task, user, account Name, role, endpoint&#160;<span style="color: rgb(51,51,51);text-decoration: none;">and all the objects defined in dynamic attributes. </span><br class="atl-forced-newline" style="text-decoration: none;text-align: left;" /></p>
                        <p class="table-text">If your target application is&#160;Azure AD, specify this parameter in the following format:</p>
                        <MadCap:codeSnippet>
                            <MadCap:codeSnippetCopyButton aria-label="Copy" title="Copy to clipboard" />
                            <MadCap:codeSnippetCaption>JSON</MadCap:codeSnippetCaption>
                            <MadCap:codeSnippetBody MadCap:useLineNumbers="False" MadCap:lineNumberStart="1" MadCap:continue="False" xml:space="preserve" style="padding-left: 12px;padding-right: 16px;padding-top: 12px;padding-bottom: 12px;mc-code-lang: SQL;">{
"call": [
{
"name": "SKU",
"connection": "userAuth",
"url": "https://graph.microsoft.com/v1.0/users/${account.accountID}/assignLicense",
"httpMethod": "POST",
"httpParams": "{\"addLicenses\": [{\"disabledPlans\": [],\"skuId\": \"${entitlementValue.entitlementID}\"}],\"removeLicenses\": []}",
"httpHeaders": {
"Authorization": "${access_token}"
},
"httpContentType": "application/json",
"successResponses": {
"statusCode": [
200,
201,
204,
205
]
}
},
{
"name": "DirectoryRole",
"connection": "userAuth",
"url": "https://graph.microsoft.com/v1.0/directoryRoles/${entitlementValue.entitlementID}/members/\\$ref",
"httpMethod": "POST",
"httpParams": "{\"@odata.id\":\"https://graph.microsoft.com/v1.0/directoryObjects/${account.accountID}\"}",
"httpHeaders": {
"Authorization": "${access_token}"
},
"httpContentType": "application/json",
"successResponses": {
"statusCode": [
200,
201,
204,
205
]
},
"unsuccessResponses": {
"odata~dot#error.code": [
"Request_BadRequest",
"Authentication_MissingOrMalformed",
"Request_ResourceNotFound",
"Authorization_RequestDenied",
"Authentication_Unauthorized"
]
}
},
{
"name": "AADGroup",
"connection": "userAuth",
"url": "https://graph.microsoft.com/v1.0/groups/${entitlementValue.entitlementID}/members/\\$ref",
"httpMethod": "POST",
"httpParams": "{\"@odata.id\":\"https://graph.microsoft.com/v1.0/directoryObjects/${account.accountID}\"}",
"httpHeaders": {
"Authorization": "${access_token}"
},
"httpContentType": "application/json",
"successResponses": {
"statusCode": [
200,
201,
204,
205
]
}
},
{
"name": "ApplicationInstance",
"connection": "entAuth",
"url": "https://graph.windows.net/myorganization/users/${account.accountID}/appRoleAssignedTo?api-version=1.6",
"httpMethod": "POST",
"httpParams": "{\"principalId\": \"${account.accountID}\", \"id\": \"${}\", \"resourceId\": \"${entitlementValue.entitlementID}\"}",
"httpHeaders": {
"Authorization": "${access_token}"
},
"httpContentType": "application/json",
"successResponses": {
"statusCode": [
200,
201,
204,
205
]
}
},
{
"name": "Team",
"connection": "userAuth",
"url": "https://graph.microsoft.com/v1.0/groups/${entitlementValue.entitlementID}/members/\\$ref",
"httpMethod": "POST",
"httpParams": "{\"@odata.id\":\"https://graph.microsoft.com/v1.0/directoryObjects/${account.accountID}\"}",
"httpHeaders": {
"Authorization": "${access_token}"
},
"httpContentType": "application/json",
"successResponses": {
"statusCode": [
200,
201,
204,
205
]
}
}
]
}</MadCap:codeSnippetBody>
                        </MadCap:codeSnippet>
                        <p class="table-text">
                            <br style="text-align: left;" />
                        </p>
                    </td>
                </tr>
                <tr class="TableStyle-Default-Table-Body-Body1">
                    <td colspan="1" class="TableStyle-Default-Table-BodyE-Column1-Body1">
                        <p class="table-text">REVOKEACCESSJSON</p>
                    </td>
                    <td colspan="1" class="TableStyle-Default-Table-BodyD-Column1-Body1">
                        <p class="table-text">Specify the command to deprovision access from the  target application.&#160;Objects exposed are task, user, account Name, role, endpoint and all the objects defined in dynamic attributes. <br class="atl-forced-newline" style="text-decoration: none;text-align: left;" /></p>
                        <p class="table-text">If your target application is&#160;Azure AD, specify this parameter in the following format:</p>
                        <MadCap:codeSnippet>
                            <MadCap:codeSnippetCopyButton aria-label="Copy" title="Copy to clipboard" />
                            <MadCap:codeSnippetCaption>JSON</MadCap:codeSnippetCaption>
                            <MadCap:codeSnippetBody MadCap:useLineNumbers="False" MadCap:lineNumberStart="1" MadCap:continue="False" xml:space="preserve" style="padding-left: 12px;padding-right: 16px;padding-top: 12px;padding-bottom: 12px;mc-code-lang: SQL;">{
"call": [
{
"name": "SKU",
"connection": "userAuth",
"url": "https://graph.microsoft.com/v1.0/users/${account.accountID}/assignLicense",
"httpMethod": "POST",
"httpParams": "{\"addLicenses\": [],\"removeLicenses\": [\"${entitlementValue.entitlementID}\"]}",
"httpHeaders": {
"Authorization": "${access_token}"
},
"httpContentType": "application/json",
"successResponses": {
"statusCode": [
200,
201,
204,
205
]
}
},
{
"name": "DirectoryRole",
"connection": "userAuth",
"url": "https://graph.microsoft.com/v1.0/directoryRoles/${entitlementValue.entitlementID}/members/${account.accountID}/\\$ref",
"httpMethod": "DELETE",
"httpHeaders": {
"Authorization": "${access_token}"
},
"httpContentType": "application/json",
"successResponses": {
"statusCode": [
200,
201,
204,
205
]
}
},
{
"name": "AADGroup",
"connection": "userAuth",
"url": "https://graph.microsoft.com/v1.0/groups/${entitlementValue.entitlementID}/members/${account.accountID}/\\$ref",
"httpMethod": "DELETE",
"httpHeaders": {
"Authorization": "${access_token}"
},
"httpContentType": "application/json",
"successResponses": {
"statusCode": [
200,
201,
204,
205
]
}
},
{
"name": "ApplicationInstance",
"connection": "entAuth",
"url": "https://graph.windows.net/myOrganization/servicePrincipals/${entitlementValue.entitlementID}/appRoleAssignedTo?api-version=1.6&amp;\\$top=999",
"httpMethod": "GET",
"httpHeaders": {
"Authorization": "${access_token}",
"Accept": "application/json"
},
"httpContentType": "application/json",
"successResponses": {
"statusCode": [
200,
201
]
}
},
{
"name": "ApplicationInstance",
"connection": "entAuth",
"url": "https://graph.windows.net/myOrganization/servicePrincipals/${entitlementValue.entitlementID}/appRoleAssignedTo/${for (Map map : response.ApplicationInstance1.message.value){if (map.principalId.toString().equals(account.accountID)){return map.objectId;}}}?api-version=1.6",
"httpMethod": "DELETE",
"httpHeaders": {
"Authorization": "${access_token}",
"Accept": "application/json"
},
"httpContentType": "application/json",
"successResponses": {
"statusCode": [
200,
201,
204,
205
]
}
}
]
}</MadCap:codeSnippetBody>
                        </MadCap:codeSnippet>
                    </td>
                </tr>
                <tr class="TableStyle-Default-Table-Body-Body1">
                    <td colspan="1" class="TableStyle-Default-Table-BodyE-Column1-Body1">
                        <p class="table-text">ENABLEACCOUNTJSON</p>
                    </td>
                    <td colspan="1" class="TableStyle-Default-Table-BodyD-Column1-Body1">
                        <p class="table-text">Specify this parameter for enabling a disabled account.&#160;Objects exposed are task, user, account name, role, endpoint and all the objects defined in dynamic attributes. <br class="atl-forced-newline" style="text-decoration: none;text-align: left;" /></p>
                        <p class="table-text">If your target application is&#160;Azure AD, specify this parameter in the following format:</p>
                        <MadCap:codeSnippet>
                            <MadCap:codeSnippetCopyButton aria-label="Copy" title="Copy to clipboard" />
                            <MadCap:codeSnippetCaption>JSON</MadCap:codeSnippetCaption>
                            <MadCap:codeSnippetBody MadCap:useLineNumbers="False" MadCap:lineNumberStart="1" MadCap:continue="False" xml:space="preserve" style="padding-left: 12px;padding-right: 16px;padding-top: 12px;padding-bottom: 12px;mc-code-lang: SQL;">{
"call": [
{
"name": "call1",
"connection": "userAuth",
"url": "https://graph.microsoft.com/v1.0/users/${account.accountID}",
"httpMethod": "PATCH",
"httpParams": "{\"accountEnabled\": true}",
"httpHeaders": {
"Authorization": "${access_token}"
},
"httpContentType": "application/json",
"successResponses": {
"statusCode": [
200,
201,
204,
205
]
}
}
]
}</MadCap:codeSnippetBody>
                        </MadCap:codeSnippet>
                    </td>
                </tr>
                <tr class="TableStyle-Default-Table-Body-Body1">
                    <td colspan="1" class="TableStyle-Default-Table-BodyB-Column1-Body1">
                        <p class="table-text">DISABLEACCOUNT</p>
                    </td>
                    <td colspan="1" class="TableStyle-Default-Table-BodyA-Column1-Body1">
                        <p class="table-text">Specify this parameter to disable an account from the target application and update that status in IGA.&#160;Objects exposed are task, user, account name, role, endpoint and all the objects defined in dynamic attributes. </p>
                        <p class="table-text">If your target application is&#160;Azure AD, specify this parameter in the following format:</p>
                        <MadCap:codeSnippet>
                            <MadCap:codeSnippetCopyButton aria-label="Copy" title="Copy to clipboard" />
                            <MadCap:codeSnippetCaption>JSON</MadCap:codeSnippetCaption>
                            <MadCap:codeSnippetBody MadCap:useLineNumbers="False" MadCap:lineNumberStart="1" MadCap:continue="False" xml:space="preserve" style="padding-left: 12px;padding-right: 16px;padding-top: 12px;padding-bottom: 12px;mc-code-lang: SQL;">{
"call": [
{
"name": "call1",
"connection": "userAuth",
"url": "https://graph.microsoft.com/v1.0/users/${account.accountID}",
"httpMethod": "PATCH",
"httpParams": "{\"accountEnabled\": false}",
"httpHeaders": {
"Authorization": "${access_token}"
},
"httpContentType": "application/json",
"successResponses": {
"statusCode": [
200,
201,
204,
205
]
}
}
]
}</MadCap:codeSnippetBody>
                        </MadCap:codeSnippet>
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="note" MadCap:autonum="&lt;b&gt;Note&lt;/b&gt;">
            <p class="note">The details specified in the connection parameters are saved in the userlogin_access table of the database. Note that values passed for username, password, token, and secret are not saved in this table because different values will be specified for them for other connections.</p>
        </div>
    </body>
</html>