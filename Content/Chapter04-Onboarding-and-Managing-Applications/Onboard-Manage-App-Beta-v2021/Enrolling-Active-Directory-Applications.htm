﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" class="concept">
    <head><title>Active Directory Applications</title>
        <link href="../../Resources/Stylesheets/Styles.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <h1>Active Directory Applications</h1>
        <p>This section provides details about enrolling an Active Directory application. After the application is enrolled, you can import the account data and perform provisioning operations on the imported accounts.</p>
        <ul>
            <li>
                <p><a href="#Prerequi">Prerequisites</a>
                </p>
            </li>
            <li>
                <p><a href="#Selectin">Selecting Features and Providing Basic Details</a>
                </p>
            </li>
            <li>
                <p><a href="#Configur">Configuring the Application Using the Quick Mode</a>
                </p>
            </li>
            <li>
                <p><a href="#Configur2">Configuring the Application Using the Assisted Mode</a>
                </p>
            </li>
            <li>
                <p><a href="#Configur3">Configuring the Application Using the Advanced Mode</a>
                </p>
            </li>
        </ul>
        <h2><a name="Prerequi"></a>Prerequisites</h2>
        <p>Before enrolling an Active Directory application, make sure that the following prerequisites are met:</p>
        <ul>
            <li>
                <p>You are familiar with Active Directory concepts.</p>
            </li>
            <li>
                <p>You have a service account in Active Directory. This service account is configured in the connection for reading and writing data to the Active Directory objects (accounts and groups). </p>
                <p MadCap:conditions="Default.v2020X"> For more information, see <b>Managing Least User Privileges for Performing Actions on Active Directory Objects</b> section in <MadCap:conditionalText MadCap:conditions="Default.Link"><a href="Integration-Requirements-at-a-Glance.htm" pubname="AD-v2020x" class="peer">Integration Requirements at a Glance</a></MadCap:conditionalText> in the <em>Microsoft Active Directory Connector Guide</em>.</p>
                <p MadCap:conditions="Default.v2021X"> For more information, see <b>Managing Least User Privileges for Performing Actions on Active Directory Objects</b> section in <MadCap:conditionalText MadCap:conditions="Default.Link"><a href="Integration-Requirements-at-a-Glance.htm" pubname="AD-v2021x" class="peer">Integration Requirements at a Glance</a></MadCap:conditionalText> in the <em>Microsoft Active Directory Connector Guide</em>.</p>
            </li>
            <li>
                <p>You have gathered the following details that are required for establishing a connection with the domain that is hosting Active Directory or for connecting to the domain controller. </p>
                <ul>
                    <li>
                        <p>The Domain Controller (DC) for Active Directory. For example: <label class="param-value">DC=saviyntazure,DC=com</label>.</p>
                    </li>
                    <li>
                        <p>The Base DN that will be used as the base search filter for searching the provisioning objects in Active Directory. For example: <label class="param-value">DC=saviyntazure,DC=com</label>.</p>
                    </li>
                </ul>
            </li>
            <li>
                <p>You have a public key certificate to setup a secure connection with Active Directory. You must export the public key certificate from Active Directory and import it to IGA.</p>
                <p MadCap:conditions="Default.v2020X"> For more information, see <b>Setting Up a Secure Connection with Active Directory</b> section in <MadCap:conditionalText MadCap:conditions="Default.Link"><a href="Understanding-the-Integration-Between-IGA-and-Active-Directory.htm" pubname="AD-v2020x" class="peer">Understanding the Integration Between IGA and AD</a></MadCap:conditionalText> in the <em>Microsoft Active Directory Connector Guide</em>.</p>
                <p MadCap:conditions="Default.v2021X"> For more information, see <b>Setting Up a Secure Connection with Active Directory</b> section in <MadCap:conditionalText MadCap:conditions="Default.Link"><a href="Understanding-the-Integration-Between-IGA-and-Active-Directory.htm" pubname="AD-v2021x" class="peer">Understanding the Integration Between IGA and AD</a></MadCap:conditionalText> in the <em>Microsoft Active Directory Connector Guide</em>.</p>
            </li>
        </ul>
        <h2><a name="Selectin"></a>Selecting Features and Providing Basic Details</h2>
        <ol>
            <li>
                <p>Log in to IGA.</p>
            </li>
            <li>
                <p>On the top right of the Home page, click&#160;<strong>Applications &gt; Design</strong>. The Application Management page is displayed. To familiarize yourself with this page, see <MadCap:xref href="Understanding-the-Interface.htm">Understanding the Interface</MadCap:xref>.</p>
            </li>
            <li>
                <p>Click the <strong>Enrollment</strong> tab. Under Enroll New Application, click <strong>Active Directory</strong>. The Enroll New Active Directory wizard is displayed.</p>
                <p>
                    <img src="../../Resources/Images/getstarted.png" style="width: 650px;" />
                </p>
            </li>
            <li>
                <p>Provide the following details for the application and click <strong>Next</strong>.</p>
                <ul>
                    <li>
                        <p><strong>Application: </strong>Specify a unique name for the application. If the specified name is not unique, you are prompted to enter a different name.</p>
                    </li>
                    <li>
                        <p><strong>Description: </strong>Specify a description for the application.</p>
                    </li>
                    <li>
                        <p><strong>Owner: </strong>Select<strong> I am the owner</strong> if you want to mark yourself as the owner of the application. Otherwise, select a different owner for the application.</p>
                        <ul>
                            <li>
                                <p>Specify whether the Owner Type of the application is a user or a user group.</p>
                            </li>
                            <li>
                                <p>Select a user or a user group as the owner of the application depending on the selected Owner Type.</p>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
                <p>In the Features page, select the features that you want to use for the application that you are enrolling. By default, the Basic configuration feature is selected. Currently, the following features are available: Account Management, Password Management, Request Configuration, Service Account Management, and Privileged Access Management. The features that are not selectable will be available in future releases. When finished, click <strong>Next</strong>.&#160;</p>
            </li>
            <li>
                <p>In the Connection page, configure the following connection details, and click <strong>Next</strong>.</p>
            </li>
        </ol>
        <ul>
            <li>
                <p><strong>Hostname: </strong>Specify the hostname or the IP address of the computer on which Active Directory is installed, to which you want to establish the connection.&#160;<br />Hostname example:&#160;<label class="param-value">hostname.domain.com</label>&#160;<br />IP address example: <label class="param-value">10.1.1.5</label>&#160;</p>
            </li>
            <li>
                <p><strong>Port: </strong>Specify the port number to use for connecting to Active Directory. The allowed port numbers are 389 and 636. Port number <label class="param-value">636</label> is set by default.</p>
            </li>
            <li>
                <p><strong>Protocol: </strong>Specify the LDAP protocol to use for connecting to Active Directory. The protocols are LDAP or LDAPS.&#160;LDAPs is set by default.</p>
            </li>
            <li>
                <p><strong>Username</strong>:&#160;Specify the username that you want to use to connect to Active Directory. It refers to the username of an account or a service account that has&#160;the necessary&#160;permissions to access Active Directory. It must be specified in the&#160;distinguishedName&#160;(DN) format.&#160;For example,<label class="param-value"> cn=Admin,cn=users,dc=saviyntdc01, dc=saviyntadmin,dc=com</label></p>
            </li>
            <li>
                <p><strong>Use Credential Vault: </strong>Enable this option if you want to use the password stored in the vault instead of providing a value for the <strong>Password</strong> field in this page. Note that it is optional to use the credential vault. This option is available only if a credential vault connection is configured in IGA. For more information, see <MadCap:xref href="Credential-Vault-Configuration.htm">Credential Vault Configuration</MadCap:xref>.</p>
            </li>
            <li>
                <p><strong>Password: </strong>Specify the password to connect to Active Directory.&#160;</p>
            </li>
            <li>
                <p><strong>Domain: </strong>Specify the DC for Active Directory. For example: <label class="param-value">(DC=saviyntazure,DC=com)</label></p>
            </li>
            <li>
                <p><strong>Base DN: </strong>Specify the base search filter for searching the provisioning objects in Active Directory. For example: <label class="param-value">DC=saviyntazure,DC=com</label>.&#160;</p>
            </li>
            <li>
                <p><strong>Test Connection: </strong>Click this button to check the connectivity with Active Directory. When the connection is successful, a confirmation message is displayed. Otherwise, a failure message is displayed. </p>
                <div class="note" MadCap:autonum="&lt;b&gt;Note&lt;/b&gt;">
                    <ul>
                        <li>
                            <p class="note">If you are using LDAPS, make sure that you have already imported Active</p>
                            <p class="note">Directory’s certificate in IGA, without which the test connection will fail.</p>
                        </li>
                        <li>
                            <p class="note">You can skip specifying the connection details and provide them later.</p>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
        <p>A new page provides a snapshot of what you selected in the wizard. It also provides options to quickly complete the enrollment or to continue configuring the application. See the subsequent sections for more information.</p>
        <h2><a name="Configur"></a>Configuring the Application Using the Quick Mode</h2>
        <ol>
            <li>
                <p>Select the features and provide the basic details of the application. For more information, see <a href="#Selectin">Selecting Features and Providing Basic Details</a>. </p>
            </li>
            <li>
                <p>On the page that provides a snapshot of what you selected in the wizard, click <strong>Complete Enrollment </strong>to enroll the application.</p>
                <p>
                    <img src="../../Resources/Images/success2.png" style="width: 450px;" />
                </p>
            </li>
        </ol>
        <p>This action performs quick enrollment by using the default settings and the few details you provided. The enrolled application is displayed in the <strong>Catalog </strong>tab of the Application Management page. You can use this page for making additional changes to the specified settings if required. For more information, see <MadCap:xref href="Understanding-the-Interface.htm#top">Understanding the Interface</MadCap:xref>.</p>
        <p>To perform import and provisioning operations, create and run the corresponding jobs for them. For more information, see <MadCap:xref href="../../Chapter10-Managing-Jobs-Using-Job-Control-Panel/Job-Control-Panel.htm#top">Job Control Panel</MadCap:xref>.</p>
        <h2><a name="Configur2"></a>Configuring the Application Using the Assisted Mode</h2>
        <ol>
            <li>
                <p>Select the features and provide the basic details of the application. For more information, see <a href="#Selectin">Selecting Features and Providing Basic Details</a>. </p>
            </li>
            <li>
                <p>On the page that provides a snapshot of what you selected in the wizard, click <strong>Continue Assisted Configuration</strong>.</p>
                <p>
                    <img src="../../Resources/Images/congratulations.png" style="width: 450px;" />
                </p>
            </li>
            <li>
                <p>Verify that <strong>Assisted configuration</strong> is selected on the Home page. </p>
                <p>
                    <img src="../../Resources/Images/assist4.png" style="width: 650px;" />
                </p>
                <p>If this option is selected, all the features selected in the <strong>Features</strong> step of the application enrollment are listed. To expand the feature list, click <strong>See More</strong>.</p>
                <div class="note" MadCap:autonum="&lt;b&gt;Note&lt;/b&gt;">
                    <p class="note">If you want to add or remove features that you want to configure, click <strong>&lt;Number of&gt; Features Selected</strong> at the top right of the <strong>Get Started</strong> section. In the Features page that displays, select or deselect the features and click <strong>Save</strong>.</p>
                </div>
            </li>
            <li>
                <p>Click the arrow next to the feature you want to configure. The configuration page of the selected feature is displayed. You can choose to follow the wizard step by step and the settings or choose a specific feature and complete the settings for it.</p>
            </li>
            <li>
                <p>Click the arrows in the left pane to navigate to the selected features that you want to configure. When finished, click <strong>Done</strong> on the left pane to complete the configuration and return to the Home page. For more information about the configuration parameters, see <MadCap:xref href="Active-Directory-Use-Cases.htm">Active Directory Use Cases</MadCap:xref>.</p>
            </li>
            <li>
                <p>To complete the enrollment, click <strong>Commit Changes</strong>. The enrolled application is displayed in the <strong>Catalog </strong>tab of the Application Management page.<br />If you do not commit the changes, the application will remain in the pending enrollment state. </p>
            </li>
            <li>
                <p>(Optional) Perform import and provisioning operations by creating and running the corresponding jobs for them. For more information, see <MadCap:xref href="../../Chapter10-Managing-Jobs-Using-Job-Control-Panel/Job-Control-Panel.htm#top">Job Control Panel</MadCap:xref>.</p>
            </li>
        </ol>
        <h2><a name="Configur3"></a>Configuring the Application Using the Advanced Mode</h2>
        <ol>
            <li>
                <p>Select the features and provide the basic details of the application. For more information, see <a href="#Selectin">Selecting Features and Providing Basic Details</a>. </p>
            </li>
            <li>
                <p>On the page that provides a snapshot of what you selected in the wizard, click <strong>Continue Assisted Configuration</strong>.</p>
                <p>
                    <img src="../../Resources/Images/assisted1.png" style="width: 450px;" />
                </p>
            </li>
            <li>
                <p>On the Home page, click <strong>Advanced configuration</strong>. The left pane displays the options for manually configuring the application. </p>
                <p>
                    <img src="../../Resources/Images/adconf2.png" style="width: 650px;" />
                </p>
            </li>
            <li>
                <p>To complete the enrollment, click <strong>Commit Changes</strong>. The enrolled application is displayed in the <strong>Catalog </strong>tab of the Application Management page. If you do not commit the changes, the application will remain in the pending enrollment state.</p>
            </li>
            <li>
                <p>(Optional) Perform import and provisioning operations by creating and running the corresponding jobs for them. For more information, see <MadCap:xref href="../../Chapter10-Managing-Jobs-Using-Job-Control-Panel/Job-Control-Panel.htm#top">Job Control Panel</MadCap:xref>.</p>
            </li>
        </ol>
    </body>
</html>