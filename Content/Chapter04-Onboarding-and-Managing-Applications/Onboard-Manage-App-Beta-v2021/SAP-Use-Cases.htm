﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head>
        <link href="../../Resources/TableStyles/Default-Table.css" rel="stylesheet" MadCap:stylesheetType="table" /><title>SAP Use Cases</title>
        <link href="../../Resources/Stylesheets/Styles.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <h1><a name="top"></a>SAP Use Cases</h1>
        <p>With SAP integration, you can monitor the access of SAP accounts in different applications in your infrastructure. You can grant application access to these accounts, manage the access, add or delete accounts, or add or delete SAP groups.</p>
        <p>You can manage SAP applications by using the Application Management page. To manage an application, the application data must be brought into IGA. This is accomplished by specifying the import configurations and running the import jobs. After the data is available, you can make changes to this data and send those changes to the SAP application. These operations are broadly known as import and provisioning in Saviynt terminology. The following sections describe some of the commonly implemented import and provisioning use cases in SAP integration.</p>
        <h2 id="SAPUseCases-ImportingData">Importing Data</h2>
        <p>You can import SAP accounts and their accesses into Saviynt. You can also import all SAP groups associated with these accounts and manage their group membership.<br />SAP grants entitlements to accounts to enable them to perform specific tasks. Entitlements are usually created in the form of role memberships or group memberships. SAP groups are imported as entitlements, which can be assigned to user accounts.  </p>
        <p>Saviynt supports two types of imports, full and incremental import. Imports are performed via jobs. For more information about jobs used for importing, see&#160;<MadCap:xref href="../../Chapter10-Managing-Jobs-Using-Job-Control-Panel/Job-Categories-for-Flat-Job-Control-Panel.htm">Job Categories for Flat Job Control Panel</MadCap:xref>.</p>
        <ul>
            <li>
                <p><strong>Full import:</strong> When you run an import job for the first time, all accounts in the SAP application are automatically imported to IGA. This is called full import. To perform the full import, the application sends import objects for each entity in the SAP application. The import process generates create or update events depending on whether the entity exists in IGA. The connector also identifies all the deleted entries and sends the deletion events to IGA.</p>
                <p>At the end of a <strong>full</strong><strong>import</strong> operation, the application typically sets the last execution time parameter to the time when the reconciliation run ends.</p>
            </li>
            <li>
                <p><strong>Incremental import:</strong> After the first full import, you can use incremental import for subsequent imports.&#160;During an incremental import, only the changes that are made in the SAP application after the last import are brought into IGA.&#160;</p>
            </li>
        </ul>
        <h3 id="SAPUseCases-ImportingSAPAccountsandAccess">Importing SAP Accounts&#160;and Access</h3>
        <p>You can import SAP accounts to IGA and associate users with accounts&#160;to gain visibility into access permissions of those accounts and manage them from IGA.&#160;Both full import (full data import) and incremental import (only import changes since the last import) are supported. </p>
        <div class="note" MadCap:autonum="&lt;b&gt;Note&lt;/b&gt;">
            <p class="note">For a successful import, Test Connection must succeed. If it fails for any reason, any import or provisioning operations will fail. For information about providing basic connection details and testing the connection, see <MadCap:xref href="Enrolling-SAP-Applications.htm" class="main-section">SAP Use Cases</MadCap:xref></p>
        </div>
        <h4 id="SAPUseCases-ImportingEntitlements"><a name="Importin"></a>Importing Entitlements</h4>
        <p>By default, IGA provides a list of SAP table names that will be imported along with the associated entitlements in the <strong>Specify table names for import</strong><strong>(relevant tables imported by default)</strong>&#160;parameter under the <strong>Setup Account import configuration</strong> section. You can add or remove the table names in this parameter based on the entitlements you want to import. However, you must list the following tables:</p>
        <ul>
            <li>
                <p>AGR_1251, AGR_1252: Authorization data</p>
            </li>
            <li>
                <p>USGRP_USER: Groups assigned to a user in SAP</p>
            </li>
            <li>
                <p>TSTC: Transaction code in SAP system</p>
            </li>
            <li>
                <p>TSTCT:&#160;tcode text data</p>
            </li>
            <li>
                <p>AGR_TCODES: Roles relation with tcode</p>
            </li>
        </ul>
        <p>Example:&#160;<span class="attr">USR02,TSTC,TSTCT,AGR_AGRS,AGR_TCODES,AGR_TEXTS,AGR_DEFINE,AGR_USERS,USORG,AGR_1252,AGR_1251,UST04,UST10C,UST10S,UST12,USGRP_USER</span></p>
        <h4 id="SAPUseCases-ConfiguringtheSystemName"><a name="Configur"></a>Configuring the System Name</h4>
        <p>In the case of a central client, specify the name of the logical system as it is configured in SAP. If this is not a central client, specify a unique name for the logical system. Configure the values in the <strong>Specify the system name to be used for import</strong> parameter under the <strong>Setup Account import configuration section</strong>.</p>
        <p>Example: EC6</p>
        <h4 id="SAPUseCases-SettingUptheStatusofImportedAccounts"><a name="Setting"></a>Setting Up the Status of Imported Accounts</h4>
        <p>Accounts in SAP have their own statuses. While import, you might need to set active status for accounts with codes, such as 512, 544, and 66048. To achieve this, specify the values of the following parameters under <strong>Status and Threshold Configuration</strong>:</p>
        <ul>
            <li>
                <p><strong>Status property in Saviynt:</strong> Specify the column name from the accounts table that stores account status values imported from SAP to IGA in string format. For example, customproperty30.</p>
            </li>
            <li>
                <p><strong>Active status:</strong> Map the status values of accounts by specifying the values of the Active statuses. For example, specify 512, 544, or 66048. If these values are found in the attribute specified in this property, say customproperty30, then they are marked active and all other values are marked inactive.</p>
            </li>
        </ul>
        <h4 id="SAPUseCases-LimitingtheNumberofInactiveAccounts"><a name="Limiting"></a>Limiting the Number of Inactive Accounts&#160;</h4>
        <p>The time taken for import is directly proportional to the volume of data that is being imported. Sometimes, the import time is increased due to connectivity issues or corruption in API calls, and in many cases, the import is interrupted or fails. An interrupted import operation means that a part of the import is not completed. </p>
        <p>When the import is run again, the accounts to import are compared with the accounts that are already imported in the previous import operation. The accounts that are already imported are updated. If the accounts are not found, they are marked Inactive or Suspended From Import Service. Such accounts cannot be made active. If you want to import the same accounts later, new accounts for them will be created in IGA. If a large number of accounts (say 10000) with Inactive or Suspended From Import Services status is detected, making them active can be a time-consuming process. </p>
        <p>Therefore, IGA allows you to define a maximum number of inactive accounts that can exist in the system. You can do this by specifying the account threshold value in the&#160;<strong>Maximum number of accounts that can be deleted during import&#160;</strong>parameter under <strong>Status and Threshold Configuration</strong>. The number of inactive accounts cannot exceed the value specified in this parameter. If the threshold value exceeds, then the status of the accounts will not change. For example, if there are 80000 active accounts and 20000 inactive accounts but the threshold value is 10000, then the statuses of the existing account data are not modified. The account statuses are updated only if the number of inactive accounts is within the limit of the threshold value, which is less than 10000.</p>
        <p>You must set the account threshold value in <strong>Maximum number of accounts that can be deleted during import</strong> by verifying the account data in your organization. In case all the data is imported but the account statuses are not updated, then check the value of this parameter and modify it according to the data in your organization. You must also update this value if the number of accounts with both active and inactive statuses has significantly increased over a period of time.</p>
        <p>In addition to specifying the account threshold value, you can configure the following for account import:</p>
        <ul>
            <li>
                <p>Remove or retain the association of entitlements for accounts. To maintain entitlement association with an account, specify <strong>false</strong> for the <strong>Remove entitlements for the missing accounts during import</strong> parameter. To remove the association, set it to <strong>true</strong>.</p>
            </li>
            <li>
                <p>Remove or retain&#160;the association of inactive accounts with users. To maintain inactive account association with a user, specify <strong>true</strong> for the <strong>Associate inactive accounts to users</strong> parameter. To remove the association, set it to <strong>false</strong>.</p>
            </li>
            <li>
                <p>Mark the missing accounts as suspended during the import. If an account is missing in Active Directory during import, its status is changed to Inactive based on the account threshold value. If you want to mark the account as Suspended From Import Service, then specify <strong>true</strong> for the <strong>Mark missing accounts as suspended during import</strong> parameter. This setting suspends the missing accounts and the account names of the suspended accounts are appended with a DELETED time stamp.</p>
            </li>
        </ul>
        <h3 id="SAPUseCases-ImportProcedure">Import Procedure</h3>
        <p>To configure importing of SAP accounts and accesses:</p>
        <ol>
            <li>
                <p>On the Home page for the application, select <strong>Assisted configuration</strong>.</p>
                <div class="note" MadCap:autonum="&lt;b&gt;Note&lt;/b&gt;">
                    <p class="note"> If you have completed the enrollment for the application, then navigate to the Home page from the <strong>Catalog</strong> tab of the Application Management page. See  for more information.</p>
                </div>
            </li>
        </ol>
        <ol MadCap:continue="true">
            <li>
                <p>Click <strong>See More</strong> to display all the selected features.<br /></p>
                <div class="note" MadCap:autonum="&lt;b&gt;Note&lt;/b&gt;">
                    <p class="note">You can add or remove the features that you want to configure by clicking <strong>&lt;Number of&gt; Features Selected</strong> on the top right of the Get Started section. In the <strong>Features </strong>page that is displayed, make the changes and click <strong>Save</strong>.</p>
                </div>
            </li>
        </ol>
        <ol MadCap:continue="true">
            <li>
                <p>To configure the application properties for account import:</p>
                <ol style="list-style-type: lower-alpha;">
                    <li>
                        <p>Under Account Management, click the right arrow next to <strong>Setup Account import configuration</strong>.</p>
                    </li>
                    <li>
                        <p>Specify values for the account import properties by referring to the sections listed in the following table:</p>
                        <div class="table-wrap">
                            <table class="TableStyle-Default-Table" style="margin-left: 0;margin-right: auto;width: 100%;mc-table-style: url('../../Resources/TableStyles/Default-Table.css');" cellspacing="0">
                                <col style="width: 340px;" class="TableStyle-Default-Table-Column-Column1" />
                                <col style="width: 340px;" class="TableStyle-Default-Table-Column-Column1" />
                                <thead>
                                    <tr class="TableStyle-Default-Table-Head-Header1">
                                        <th class="TableStyle-Default-Table-HeadE-Column1-Header1">
                                            <p class="table-text">Property</p>
                                        </th>
                                        <th class="TableStyle-Default-Table-HeadD-Column1-Header1">
                                            <p class="table-text">See More…</p>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="TableStyle-Default-Table-Body-Body1">
                                        <td class="TableStyle-Default-Table-BodyE-Column1-Body1">
                                            <p class="table-text">Specify table names for import (relevant tables imported by default)</p>
                                        </td>
                                        <td class="TableStyle-Default-Table-BodyD-Column1-Body1">
                                            <p class="table-text">
                                                <MadCap:xref href="#Importin">Importing Entitlements</MadCap:xref>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr class="TableStyle-Default-Table-Body-Body1">
                                        <td class="TableStyle-Default-Table-BodyE-Column1-Body1">
                                            <p class="table-text">Specify the system name to be used for import</p>
                                        </td>
                                        <td class="TableStyle-Default-Table-BodyD-Column1-Body1">
                                            <p class="table-text">
                                                <MadCap:xref href="#Configur">Configuring the System Name</MadCap:xref>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr class="TableStyle-Default-Table-Body-Body1">
                                        <td class="TableStyle-Default-Table-BodyB-Column1-Body1">
                                            <p class="table-text">Status and Threshold Configuration</p>
                                        </td>
                                        <td class="TableStyle-Default-Table-BodyA-Column1-Body1">
                                            <p class="table-text">
                                                <MadCap:xref href="#Setting">Setting Up the Status of Imported Accounts</MadCap:xref>
                                            </p>
                                            <p class="table-text">
                                                <MadCap:xref href="#Limiting">Limiting the Number of Inactive Accounts&#160;</MadCap:xref>
                                            </p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </li>
                    <li>
                        <p>When finished, click <strong>Done</strong> on the left pane to complete the configuration and return to the Home page. </p>
                    </li>
                </ol>
            </li>
            <li>
                <p>To complete enrolling the application, click <strong>Commit Changes</strong>.</p>
            </li>
            <li>
                <p>Add a job of the <strong>Application Data Import</strong> type. In the created trigger, select <strong>Import Type</strong> as <strong>Accounts</strong>, and run the job. For more information about creating and running jobs, see <MadCap:xref href="../../Chapter10-Managing-Jobs-Using-Job-Control-Panel/Job-Control-Panel.htm">Job Control Panel</MadCap:xref>.</p>
                <p>After the job completes, the accounts will be imported into the Identity Repository of IGA.</p>
            </li>
        </ol>
        <h2 id="SAPUseCases-ProvisioningData">Provisioning Data</h2>
        <p>Provisioning automates user and account creation, and helps you manage access privileges through the user lifecycle.&#160;You can create, read, and update user accounts for new or existing users, and remove accounts for deactivated users.</p>
        <p>The application onboarding and management feature also enables you to perform group assignment provisioning operations in which you set or change the target system group membership profiles of user accounts.</p>
        <h3 id="SAPUseCases-ProvisioningSAPAccounts">Provisioning SAP Accounts&#160;</h3>
        <p>Provisioning involves creating, updating, or deleting accounts in SAP through Saviynt. When you provision an SAP account to a user, the operation results in the creation of an account for that user in SAP. You can also update existing SAP accounts through Saviynt.</p>
        <h4 id="SAPUseCases-CreatinganAccount"><a name="Creating"></a>Creating an Account&#160;</h4>
        <p>You can create accounts in SAP&#160;by navigating to the <strong>Setup Account Provisioning configuration</strong> section and specifying the values in the<strong>&#160;Create Account Policy</strong>&#160;parameter.</p>
        <div class="note" MadCap:autonum="&lt;b&gt;Note&lt;/b&gt;">
            <p class="note">You can click <strong>Switch to Editor View</strong> to display the account attribute mapping in JSON format. After modifying the mapping, click <strong>Switch to Builder View</strong> to display it in this view.</p>
        </div>
        <p>To define the attributes to be used while creating an account, expand <strong>Create Account Policy</strong>, and specify values for the <span class="inline-comment-marker">parameters</span> by referring to the links to SAP documentation provided in the following table:</p>
        <div class="table-wrap">
            <table class="TableStyle-Default-Table" style="width: 100%;margin-left: 0;margin-right: auto;mc-table-style: url('../../Resources/TableStyles/Default-Table.css');" cellspacing="0">
                <col style="width: 199.0px;" class="TableStyle-Default-Table-Column-Column1" />
                <col style="width: 481.0px;" class="TableStyle-Default-Table-Column-Column1" />
                <thead>
                    <tr class="TableStyle-Default-Table-Head-Header1">
                        <th class="TableStyle-Default-Table-HeadE-Column1-Header1">Parameter</th>
                        <th class="TableStyle-Default-Table-HeadD-Column1-Header1"><strong>See More...</strong>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="TableStyle-Default-Table-Body-Body1">
                        <td class="TableStyle-Default-Table-BodyE-Column1-Body1">
                            <p><span class="inline-comment-marker">ADDRESS</span>
                            </p>
                        </td>
                        <td class="TableStyle-Default-Table-BodyD-Column1-Body1">
                            <p><a href="https://www.se80.co.uk/saptables/b/bapi/bapiaddr3.htm" class="external-link" rel="nofollow">BAPIADDR3 SAP BAPI reference structure for addresses (contact person) Structure and data</a>
                            </p>
                        </td>
                    </tr>
                    <tr class="TableStyle-Default-Table-Body-Body1">
                        <td class="TableStyle-Default-Table-BodyE-Column1-Body1">
                            <p><span class="inline-comment-marker">DEFAULTS</span>
                            </p>
                        </td>
                        <td class="TableStyle-Default-Table-BodyD-Column1-Body1">
                            <p><a href="https://www.se80.co.uk/saptables/b/bapi/bapidefaul.htm" class="external-link" rel="nofollow">BAPIDEFAUL SAP User: Fixed Values Transfer Structure Structure and data</a>
                            </p>
                        </td>
                    </tr>
                    <tr class="TableStyle-Default-Table-Body-Body1">
                        <td class="TableStyle-Default-Table-BodyE-Column1-Body1">
                            <p><span class="inline-comment-marker">PROFILES</span>
                            </p>
                        </td>
                        <td class="TableStyle-Default-Table-BodyD-Column1-Body1">
                            <p><a href="https://www.se80.co.uk/saptables/b/bapi/bapiprof.htm" class="external-link" rel="nofollow">BAPIPROF SAP User: Profile Transfer Structure Structure and data</a>
                            </p>
                        </td>
                    </tr>
                    <tr class="TableStyle-Default-Table-Body-Body1">
                        <td class="TableStyle-Default-Table-BodyE-Column1-Body1">
                            <p><span class="inline-comment-marker">REF_USER</span>
                            </p>
                        </td>
                        <td class="TableStyle-Default-Table-BodyD-Column1-Body1">
                            <p><a href="https://www.se80.co.uk/saptables/b/bapi/bapirefus.htm" class="external-link" rel="nofollow">BAPIREFUS SAP User name Structure and data</a>
                            </p>
                        </td>
                    </tr>
                    <tr class="TableStyle-Default-Table-Body-Body1">
                        <td class="TableStyle-Default-Table-BodyE-Column1-Body1">
                            <p><span class="inline-comment-marker">LOGONDATA</span>
                            </p>
                        </td>
                        <td class="TableStyle-Default-Table-BodyD-Column1-Body1">
                            <p><a href="https://www.se80.co.uk/saptables/b/bapi/bapilogond.htm" class="external-link" rel="nofollow">BAPILOGOND SAP User: Logon Data Transfer Structure Structure and data</a>
                            </p>
                        </td>
                    </tr>
                    <tr class="TableStyle-Default-Table-Body-Body1">
                        <td class="TableStyle-Default-Table-BodyE-Column1-Body1">
                            <p><span class="inline-comment-marker">PASSWORD</span>
                            </p>
                        </td>
                        <td class="TableStyle-Default-Table-BodyD-Column1-Body1">
                            <p><a href="https://www.se80.co.uk/saptables/b/bapi/bapipwd.htm" class="external-link" rel="nofollow">BAPIPWD SAP SAP user password Structure and data</a>
                            </p>
                        </td>
                    </tr>
                    <tr class="TableStyle-Default-Table-Body-Body1">
                        <td class="TableStyle-Default-Table-BodyB-Column1-Body1">
                            <p><span class="inline-comment-marker"><span class="inline-comment-marker">PARAMETER</span></span>
                            </p>
                        </td>
                        <td class="TableStyle-Default-Table-BodyA-Column1-Body1">
                            <p><a href="https://www.se80.co.uk/saptables/b/bapi/bapiparam.htm" class="external-link" rel="nofollow">BAPIPARAM SAP User: Parameter Transfer Structure Structure and data</a>
                            </p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <p>Perform the following steps if you want to add additional attributes:</p>
        <ol>
            <li>
                <p>Click <strong>Add Attributes</strong> and type the name of the attribute you want to add. For information about the attributes you can add, see <a href="https://www.se80.co.uk/sapfms/b/bapi/bapi_user_create1.htm"><p>BAPI_USER_CREATE1 SAP Function module</p></a></p>
            </li>
            <li>
                <p>Under Create New, click the attribute name you entered in step 1. The Create &lt;Attribute_Name&gt; as New dialog box is displayed.</p>
            </li>
            <li>
                <p>From the Select input format drop down, select <strong>Single Value</strong> or <strong>Multi Value</strong>.</p>
            </li>
            <li>
                <p>From the Set Data Type drop down, select a data type, such as string, number, or boolean.</p>
            </li>
            <li>
                <p>Click <strong>Create</strong>.</p>
            </li>
        </ol>
        <h4 id="SAPUseCases-UpdatinganAccount"><a name="Updating"></a>Updating an Account&#160;</h4>
        <p>You can update the details of an existing account by specifying the<strong>&#160;Update Account Policy&#160;</strong>parameter.&#160;For more information about the attributes see <MadCap:xref href="../5.5.x/Creating-Accounts.htm">Creating Accounts</MadCap:xref>.&#160;</p>
        <h4 id="SAPUseCases-EnablinganAccount"><a name="Enabling"></a>Enabling an Account&#160;</h4>
        <p>You can enable a disabled account by specifying the&#160;<strong>Enable Account Policy</strong>&#160;parameter.&#160;</p>
        <p>The application onboarding and management feature uses the values specified for this parameter to check the attributes associated with the account before enabling it.&#160;For more information about the attributes see <MadCap:xref href="../5.5.x/Creating-Accounts.htm">Creating Accounts</MadCap:xref>.&#160;</p>
        <div class="note" MadCap:autonum="&lt;b&gt;Note&lt;/b&gt;">
            <p class="note">You can click <strong>Switch to Editor View</strong> to display the group attribute mapping in JSON format. You can modify the JSON. Clicking <strong>Switch to Builder View</strong> displays the attributes in the mapping.</p>
        </div>
        <h3 id="SAPUseCases-ProvisioningProcedure">Provisioning Procedure&#160;</h3>
        <p>To configure provisioning of SAP accounts and accesses:</p>
        <ol>
            <li>
                <p>On the <strong>Home </strong>page, select <strong>Assisted configuration</strong>.</p>
            </li>
            <li>
                <p>Click <strong>See More</strong> to display all the selected features.<br /></p>
                <div class="note" MadCap:autonum="&lt;b&gt;Note&lt;/b&gt;">
                    <p class="note">You can add or remove the features you want to configure by clicking <strong>&lt;Number of&gt; Features Selected</strong> on the top right of the Get Started section. In the <strong>Features </strong>page that is displayed, make the changes and click <strong>Save</strong>.</p>
                </div>
            </li>
            <li>
                <p>To configure the application properties used for account provisioning:</p>
                <ol style="list-style-type: lower-alpha;">
                    <li>
                        <p>Under Account Management, click the right arrow next to <strong>Setup Account Provisioning Configuration</strong>.</p>
                    </li>
                    <li>
                        <p>Specify values for the account provisioning properties by referring to the sections listed in the following table:</p>
                        <div class="table-wrap">
                            <table class="TableStyle-Default-Table" style="width: 100%;margin-left: 0;margin-right: auto;mc-table-style: url('../../Resources/TableStyles/Default-Table.css');" cellspacing="0">
                                <col style="width: 340px;" class="TableStyle-Default-Table-Column-Column1" />
                                <col style="width: 340px;" class="TableStyle-Default-Table-Column-Column1" />
                                <thead>
                                    <tr class="TableStyle-Default-Table-Head-Header1">
                                        <th class="TableStyle-Default-Table-HeadE-Column1-Header1">Property</th>
                                        <th class="TableStyle-Default-Table-HeadD-Column1-Header1">See More...</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="TableStyle-Default-Table-Body-Body1">
                                        <td class="TableStyle-Default-Table-BodyE-Column1-Body1">
                                            <p class="table-text">Create Account Policy</p>
                                        </td>
                                        <td class="TableStyle-Default-Table-BodyD-Column1-Body1">
                                            <p class="table-text">
                                                <MadCap:xref href="#Creating">Creating an Account&#160;</MadCap:xref>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr class="TableStyle-Default-Table-Body-Body1">
                                        <td class="TableStyle-Default-Table-BodyE-Column1-Body1">
                                            <p class="table-text">Update Account Policy</p>
                                        </td>
                                        <td class="TableStyle-Default-Table-BodyD-Column1-Body1">
                                            <p class="table-text">
                                                <MadCap:xref href="#Updating">Updating an Account&#160;</MadCap:xref>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr class="TableStyle-Default-Table-Body-Body1">
                                        <td class="TableStyle-Default-Table-BodyB-Column1-Body1">
                                            <p class="table-text">Enable Account Policy</p>
                                        </td>
                                        <td class="TableStyle-Default-Table-BodyA-Column1-Body1">
                                            <p class="table-text">
                                                <MadCap:xref href="#Enabling">Enabling an Account&#160;</MadCap:xref>
                                            </p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </li>
                    <li>
                        <p>When finished, click <strong>Done</strong> on the left pane to complete the configuration and return to the Home page. </p>
                    </li>
                </ol>
            </li>
            <li>
                <p>To complete enrolling the application, click <strong>Commit Changes</strong>.&#160;</p>
            </li>
            <li>
                <p>Configure the&#160;<strong>Provisioning&#160;</strong>job<strong>&#160;(WSRETRY)</strong>.&#160;For more information, see <em>Utility </em>section in <MadCap:xref href="../../Chapter10-Managing-Jobs-Using-Job-Control-Panel/Job-Categories-for-Flat-Job-Control-Panel.htm">Job Categories for Flat Job Control Panel</MadCap:xref>.</p>
                <p>When a provisioning job is triggered, it creates provisioning tasks in IGA. When these tasks are completed, the provisioning action is performed on the target application.&#160;</p>
            </li>
        </ol>
        <h2 id="SAPUseCases-ManagingServiceAccounts"><a name="Managing2"></a>Managing Service Accounts</h2>
        <p>A service account is an account&#160;in an application with privileged&#160;responsibilities and is provided for a temporary period to users.&#160;Every service account in IGA has at least one owner who has permission to manage these accounts. As a service account owner, you can perform different actions on existing service accounts owned by you.&#160;For more information, see <MadCap:xref href="#Managing2">Managing Service Accounts</MadCap:xref>.</p>
        <h2 id="SAPUseCases-ManagingRequests"><a name="Managing"></a>Managing Requests</h2>
        <p>IGA provides you centralized identity management and governance capabilities. This helps you to build a governance model for your enterprise for better visibility into who has access to what. An administrator defines roles and permissions for users based on their requirements. The approvers analyze the criticality or risk of granting access permissions and take the right action of granting or denying access that users do not need, such as when users change positions in a company and inadvertently accrue too many privileges. For more information, see <MadCap:xref href="#Managing">Managing Requests</MadCap:xref>.</p>
    </body>
</html>