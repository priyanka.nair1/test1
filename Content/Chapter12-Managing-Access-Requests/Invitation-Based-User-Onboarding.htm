﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" class="task">
    <head><title>Invitation-Based User Onboarding</title>
        <link href="../Resources/Stylesheets/Styles.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <h1>Invitation-Based User Onboarding</h1>
        <p>Invitation-based user onboarding is the process of onboarding employees and non-employees such as guests, vendors, contractors, partners, and part-time employees&#160;that are rarely directly managed by the organization&#160;as organizational identities. When these users spread across departments and locations of the organization, it becomes critical to track permissions and access rights associated with them. With the growth of organizations, more non-employees are hired and the need to provide ease of onboarding them and subsequently providing them the required accesses without compromising security becomes even more important. <br />Efficient management of onboarding and offboarding non-employees involves who is accessing what, and when, where, and how they access it. If the organization has these details for every employee, it can determine the permissions that the individuals must have, and how to manage them for the tenure of their employment. Saviynt Identity Governance and Administration (IGA) provides an ability to onboard non-employee users and manage their life cycle, including access to roles, resources, endpoints, and so on.</p>
        <h2 id="Invitation-BasedUserOnboarding-MethodstoOnboardUsers">Methods to Onboard Users</h2>
        <p>IGA provides the following ways to onboard users, including non-employee users from third-party organizations:</p>
        <ul>
            <li>
                <p>Direct onboarding by an administrator or a manager</p>
            </li>
            <li>
                <p>Importing from an external system</p>
            </li>
            <li>
                <p>Inviting users to register themselves</p>
            </li>
        </ul>
        <p>This section describes the process for inviting users to register themselves, which is commonly referred to as invitation-based user onboarding. </p>
        <p MadCap:conditions="Default.v2020X">For details about other methods, see <MadCap:conditionalText MadCap:conditions="Default.Link"><a href="ars-cre-usr.htm" pubname="IGA-User-Guide-v2020x" class="peer">Creating Users</a></MadCap:conditionalText> in the <em>IGA User Guide</em> and <MadCap:xref href="../Chapter03-Onboarding-Users/Importing-Users-using-the-UserImport-Job.htm#top">Importing Users using the UserImport Job</MadCap:xref>.</p>
        <p MadCap:conditions="Default.v2021X">For details about other methods, see <MadCap:conditionalText MadCap:conditions="Default.Link"><a href="ars-cre-usr.htm" pubname="IGA-User-Guide-v2021x" class="peer">Creating Users</a></MadCap:conditionalText> in the <em>IGA User Guide</em> and <MadCap:xref href="../Chapter03-Onboarding-Users/Importing-Users-using-the-UserImport-Job.htm#top">Importing Users using the UserImport Job</MadCap:xref>.</p>
        <h3 id="Invitation-BasedUserOnboarding-UnderstandingInvitation-BasedUserOnboarding">Understanding Invitation-Based User Onboarding</h3>
        <p>IGA provides an out-of-the-box capability for invitation-based onboarding of users where users are invited to register themselves for onboarding. The process is initiated with minimal information that requires only the email address of the invitee user. The invitee user receives an invitation email that contains a link to an un-authenticated yet secure user registration form that must be filled in with the required user details. </p>
        <p>Managing the non-employee lifecycle involves the following participants:<br /><strong>Sponsor</strong>: Initiates an invite to onboard a guest user into the system. The sponsor is responsible for managing the guest’s accesses.</p>
        <p><strong>Guest</strong>: Invitee such as vendor, contractor, partner, or any non-employee user who needs short-term access to the system.</p>
        <h2 id="Invitation-BasedUserOnboarding-ConfiguringInvitation-BasedUserOnboarding">Configuring Invitation-Based User Onboarding</h2>
        <p>To setup invitation-based user onboarding, perform the following steps:</p>
        <ol>
            <li>
                <p>Set up invitation forms.&#160;</p>
                <ul>
                    <li>
                        <p><strong>Inviter Form</strong>: This form must be filled by a sponsor or an administrator to invite a guest user. </p>
                    </li>
                    <li>
                        <p><strong>Invitee Form</strong>: The form must be filled and submitted by a guest user.</p>
                    </li>
                </ul>
            </li>
        </ol>
        <p style="margin-left: 30.0px;">You can configure both forms using dynamic attributes. Go to <strong>Admin &gt; Global Configuration &gt; Identity Lifecycle &gt; Dynamic Attributes</strong> if you are using the new Global Configuration page. Otherwise, go to <strong>Admin &gt; Configurations &gt; Global Configuration &gt; Register User &gt; Dynamic Attributes </strong>if you are using the old Global Configuration page. For more information, see <MadCap:xref href="../Chapter06-Configuring-EIC/Identity-Lifecycle-Configuration.htm">Identity Lifecycle Configuration</MadCap:xref>. </p>
        <p>2. Configure email templates.</p>
        <ul>
            <li>
                <p><strong>Invitation Email</strong>: This is a mandatory email template that is configured as the <strong>Notification Email Template&#160;</strong>in the workflow&#160;Invite&#160;activity. The email contains the unique URL of the secure user registration form that the invitee receives. Ensure that the email contains the following binding variable that substitutes the URL of the end-user Registration Form that is automatically triggered.<br /><em>&lt;https://&lt;Domain Name&gt;/ECM/home/external?token=${token}</em></p>
            </li>
            <li>
                <p><strong>Notification Email</strong>: This is an optional email template. If you want to send a notification email to another user while sending out an invitation; typically, the sponsor or the manager of the invitee, configure the&#160;<strong>Notification Email Template (Requestor)&#160;</strong>in the workflow&#160;Invite&#160;activity.</p>
            </li>
            <li>
                <p><strong>Reminder Email</strong>:&#160;This is an optional email template. If you want to send a reminder to an invitee who has not acted upon the invitation, configure this template as the&#160;1st  &#160;<strong>Reminder Email Template&#160;</strong>in the workflow&#160;Invite&#160;activity.<br />For general information about email templates, see <MadCap:xref href="../Chapter06-Configuring-EIC/Creating-and-Managing-Email-Templates.htm">Creating and Managing Email Templates</MadCap:xref>.</p>
            </li>
        </ul>
        <p>3. Configure workflows.<br />You must create a workflow and add the <strong>Invite</strong> activity to it. This activity manages the invitation to a guest user. It generates a secure URL for the end-user registration form (invitation form) and sends out an invitation email containing the URL. </p>
        <p>To configure the workflow <strong>Invite</strong> activity, <span class="inline-comment-marker">perform the following steps:</span></p>
        <ol>
            <li>
                <p>Create a workflow.</p>
            </li>
            <li>
                <p>Select <strong>Invite Action </strong>in the workflow.</p>
            </li>
            <li>
                <p>Fill in the values for the following parameters for the <strong>Invite Action</strong> activity:</p>
                <ul>
                    <li>
                        <p><strong>Name</strong>: Specify the name of the activity in the text field.</p>
                    </li>
                    <li>
                        <p><strong>Notification Email Template (Invitee)</strong>: Select the email template to use for sending the invite to the <span class="inline-comment-marker">invitee.</span> This is a mandatory field.</p>
                    </li>
                    <li>
                        <p><strong>Notification Email Template (Requester)</strong>: Select the email template to use for sending <span class="inline-comment-marker">notification to other participants, typically a Sponsor or a Manager, about sending an invite to the invitee.</span> </p>
                    </li>
                    <li>
                        <p><strong>1st Reminder Email Template</strong>: Select the email template to send as the first reminder to an invitee.</p>
                    </li>
                    <li>
                        <p><strong>1st Reminder Due Date</strong>: Specify the time interval after which the reminder email is available for sending to the invitee. </p>
                    </li>
                </ul>
            </li>
        </ol>
        <p style="margin-left: 30.0px;"><span class="inline-comment-marker">After submitting the values, an email invitation is sent to the user with a unique link to the unauthenticated User Registration page. You can modify the email content through notification email templates. However, the link to the User Registration form is system generated and substituted for the binding variable contained in </span>Notification Email Template (Invitee)<span class="inline-comment-marker">.</span><span class="inline-comment-marker">For more information about email templates, </span>see <MadCap:xref href="../Chapter06-Configuring-EIC/Creating-and-Managing-Email-Templates.htm">Creating and Managing Email Templates.</MadCap:xref></p><span class="confluence-embedded-file-wrapper image-center-wrapper confluence-embedded-manual-size"><img class="confluence-embedded-image confluence-external-resource image-center" width="646" src="https://lh3.googleusercontent.com/-35S7cXMojbM/XeX76alh60I/AAAAAAAAAGg/qngeESNaLPQyTdmApDPwwW25sVLTTNi3wCK8BGAsYHg/s0/2019-12-02.png" loading="lazy" /></span>
        <p>Figure: Sample Workflow</p>
        <h2 id="Invitation-BasedUserOnboarding-AdditionalConfiguration">Additional Configuration</h2>
        <p>The Global Configurations page for Register User or Identity Lifecycle provides configuration parameters that allow you to manage security settings and acknowledgment options (for terms and conditions) for invitation-based onboarding. These options are listed below for your reference:</p>
        <ul>
            <li>
                <p><strong>Invitation Time-To-Live</strong>: The amount of&#160;time (in minutes)&#160;for which an Invite URL exists before it expires. If the configured time&#160;elapses, the guest user is no longer taken to the User Registration page.</p>
            </li>
            <li>
                <p><strong>Invite URL Maximum Retry Count</strong>: The maximum number of times a guest user can try to open the Invite URL.&#160;</p>
            </li>
            <li>
                <p><strong>Acknowledge Checkbox for Create User</strong>: Enables the <strong>Acknowledgment </strong>option in the <strong>Create User Request&#160;</strong>(Inviter) form.</p>
            </li>
            <li>
                <p><strong>Acknowledge Checkbox for Invitation Form</strong>: Enables the <strong>Acknowledgment </strong>option in the guest user registration form.</p>
            </li>
        </ul>
        <p>For more information about these parameters, see <MadCap:xref href="../Chapter06-Configuring-EIC/Identity-Lifecycle-Configuration.htm#top">Identity Lifecycle Configuration</MadCap:xref>.</p>
    </body>
</html>