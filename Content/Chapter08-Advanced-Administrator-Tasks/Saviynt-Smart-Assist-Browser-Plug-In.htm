﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head><title>Saviynt Smart Assist Browser Plug-In</title>
        <link href="../Resources/Stylesheets/Styles.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <h1>Saviynt Smart Assist Browser Plug-In</h1>
        <p>Saviynt’s Smart Assist Browser plug-in streamlines the process of obtaining access to applications managed by Saviynt Identity Governance and Administration (IGA). It enables a user to request access for the required application directly from the browser extension instead of logging in to the Saviynt application. The browser plug-in scans the whitelisted application URLs that are restricted for a user and recommends the application accesses based on the user's profile. This functionality is currently limited to a few web applications in this release.</p>
        <p>The Smart Assist browser plug-in provides the following functionality:</p>
        <ul>
            <li>
                <p><strong>Ease of Authentication:</strong> Authenticates the user’s sign-in using the user credentials.</p>
            </li>
            <li>
                <p><strong>Ease of browsing for long sessions:</strong> Ensures that the user doesn’t need to re-authenticate during a long session. The authentication is only needed when the session is restarted.</p>
            </li>
            <li>
                <p><strong>Intelligent Access for Applications</strong>: Checks if the user has the required access permissions for a specific application, and informs the user of the required permissions if the access rights aren’t found. The plug-in provides a list of recommendations based on the user’s current access and the applications that are being accessed.</p>
            </li>
        </ul>
        <ul>
            <li>
                <p><strong>Ease of Request Access for Applications:</strong> Creates the request ID for an entitlement that can be viewed by the user for applications where the user doesn’t have access. </p>
            </li>
        </ul>
        <h2 id="SaviyntSmartAssistBrowserPlug-In-InstallingtheSmartAssistBrowserPlug-In">Installing the Smart Assist Browser Plug-In </h2>
        <p>Perform the following steps to install the Smart Assist browser plug-in in your Chrome browser:</p>
        <ol>
            <li>
                <p>Drag and drop the SmartAssist setup file to your browser’s extension page to install the browser plug-in. To download the setup file, see the <strong>Smart Assist Browser Plug-In</strong> line item on the <MadCap:conditionalText MadCap:conditions="Default.Link"><a href="Saviynt-Enterprise-Identity-Cloud-Artifacts.htm" pubname="EIC-Artifacts" class="peer">EIC Artifacts</a></MadCap:conditionalText>.</p>
            </li>
        </ol>
        <div class="important">
            <p class="note">The browser plug-in currently supports the Google Chrome browser. You need to enable the developer mode to install the Smart Assist plug-in. Click the&#160;<strong>Chrome</strong>&#160;menu icon and select <strong>Extensions</strong> from the <strong>Tools</strong> menu. Ensure that the "<strong>Developer mode</strong>" checkbox in the top right-hand corner is checked. Now you can reload extensions, load an unpacked directory of files as if it were a packaged extension.</p>
        </div>
        <div class="note" MadCap:autonum="&lt;b&gt;Note&lt;/b&gt;">
            <p class="note">The SSA package contains configurations such as IGA server URL, plug-in logo, plug-in header, and description, and can be used for all operating systems. IGA server URL is present in the <label class="filename-path">properties.json</label> file located in the <strong>res</strong> folder within the <strong>SaviyntSmartAssist</strong> directory.</p>
        </div>
        <ol MadCap:continue="true">
            <li>
                <p>Once the plugin is installed, it will ask you to enter your IGA login credentials as displayed below.</p>
                <p>
                    <img src="../Resources/Images/SaviyntSmartAssist.png" style="width: 700px;height: 800px;" />
                </p>
                <p class="note">You will remain signed-in into this plugin unless you sign out.</p>
            </li>
        </ol>
        <ol MadCap:continue="true">
            <li>
                <p>The plugin confirms your login as displayed in the following figure:</p>
                <p>
                    <img src="../Resources/Images/Saviynt smart assist1.png" style="width: 700px;height: 800px;" />
                </p>
            </li>
        </ol>
        <h2 id="SaviyntSmartAssistBrowserPlug-In-ConfigurationsNeededinEIC">Configurations Needed in IGA</h2>
        <p>Perform the following steps to configure IGA for the Smart Assist browser plugin:</p>
        <ol>
            <li>
                <p>Define a list of whitelist URLs for an endpoint in the <strong>Plugin Configuration </strong>field available on the Endpoint details page as displayed in the following image.</p>
                <p>
                    <img src="../Resources/Images/Plugin Configuration1.png" style="width: 1200px;height: auto;" />
                </p>
            </li>
        </ol>
        <div class="important">
            <p class="note">This field accepts JSON value. A JSON sample is shown below:</p>
        </div>
        <MadCap:codeSnippet>
            <MadCap:codeSnippetCopyButton aria-label="Copy" title="Copy to clipboard" />
            <MadCap:codeSnippetCaption>JSON</MadCap:codeSnippetCaption>
            <MadCap:codeSnippetBody MadCap:useLineNumbers="False" MadCap:lineNumberStart="1" MadCap:continue="False" xml:space="preserve" style="padding-left: 12px;padding-right: 16px;padding-top: 12px;padding-bottom: 12px;mc-code-lang: SQL;">{
"features": [
{
"type": "ACCESS",
"url": [
"salesforce.com"
],
"maxRecomendationCount": -1,

"createaccountifnotexists":"true",

"checksod":"false",

"accountnamefromrule":"true"

}
]
}</MadCap:codeSnippetBody>
        </MadCap:codeSnippet>
        <ol MadCap:continue="true">
            <li>
                <p>Define the JRM rule under Roles based on which the recommendations will be provided. Please see <MadCap:xref href="Overview-of-Peer-Access-Analytics.htm">Peer Access Analytics</MadCap:xref> for further information.</p>
            </li>
        </ol>
        <ol MadCap:continue="true">
            <li>
                <p>Within Endpoint, select the entitlement types that must be requested, as all entitlements within those are a part of the recommendations and can be requested.</p>
            </li>
        </ol>
        <h2 id="SaviyntSmartAssistBrowserPlug-In-HowtheBrowserPlug-InWorks">How the Browser Plug-In Works</h2>
        <p>The browser plug-in initializes with user authentication and provides recommendations based on the browsing session of the user. This is described in the following steps:</p>
        <ol>
            <li>
                <p>The plug-in uses an encrypted SSL connection to obtain authentication information and other required information from IGA and then applies that information to the browser page. </p>
            </li>
            <li>
                <p>The plug-in fetches and stores the whitelisted URLs metadata from IGA for a user based on the plugin configuration defined for all endpoints. </p>
            </li>
            <li>
                <p>Based on the browsing session of the logged-in user, the plug-in compares the URL entered by the user in the browser against whitelisted URLs obtained in previous steps. If a match is found, the plug-in checks the access status of the user for that application. If the user doesn’t have access, the plug-in fetches applicable recommendations and displays it to the user as displayed in the following figure:</p>
                <p>
                    <img src="../Resources/Images/request access to slack.png" style="width: 700px;" />
                </p>
            </li>
        </ol>
        <ol MadCap:continue="true">
            <li>
                <p>The user can then select the options among the recommended items and click <strong>Request</strong> to submit the request for those entitlements. The plug-in displays the Access Request Submitted pop-up along with the associated ticket number and hyperlink of the access request to track the access request as displayed in the following figure:</p>
                <p>
                    <img src="../Resources/Images/request access to slack1.png" style="width: 700px;" />
                </p>
                <p>Prior to the submission of the access request, the user can choose to cancel an entitlement by clicking on the cross icon displayed adjacent to the recommended item.</p>
            </li>
            <li>
                <p>The plug-in generates access requests based on the applicable entitlements on behalf of the user and displays corresponding requests IDs to the user. The plug-in keeps checking the status of the request IDs and if confirmed, displays a successful provisioning message to the user.</p>
                <p>To enhance security, the plug-in only works with trusted and verified sites. </p>
            </li>
        </ol>
    </body>
</html>