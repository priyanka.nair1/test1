﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head><title>My Uploads</title>
        <link href="../Resources/Stylesheets/Styles.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <h1>My Uploads</h1>
        <h2 id="MyUploads-Overview">Overview</h2>
        <p>Using the new upload framework introduced from Release v2020.0 onwards, upload for a large amount of data is less time-consuming and it no longer results in a hung state. When an upload is performed, a background job is run that now performs data validation in the background. Using the new icon ‘<strong>Upload Files</strong>’ available at the top navigation panel, you can view the upload status using the <strong>Uploaded Reports </strong>page. You can track the status of uploads with status such as: ‘Pending’, ‘In-Progress’, ‘Validation-Failed’, ‘Failed’, and ‘Completed’. You can also view the upload summary and download the report to identify which specific user or role upload process has failed. </p>
        <h2 id="MyUploads-Prerequisites">Prerequisites</h2>
        <p>The prerequisites to use the new upload framework are:</p>
        <ul>
            <li>
                <p>Select the ‘<strong>Upload Files</strong>’ option using <strong>Global Configurations &gt; Preferences &gt; Notifications</strong> to display the ‘Upload Files’ icon on the top navigation panel to view the <strong>Uploaded Report </strong>on the UI.</p>
            </li>
            <li>
                <p>Select the ‘<strong>Enable Upload Framework (Beta)</strong>’ option using <strong>Global Configurations &gt; Preferences &gt; Features </strong>to process bulk-user and role uploads process using a background job. You will see the ‘<strong>Run Now</strong>’ option instead of <strong>Preview</strong> for the Bulk-user upload if you have selected the ‘<strong>Enable Upload Framework (Beta)</strong>’ option.  </p>
            </li>
            <li>
                <p>Select the ‘<strong>Enable Refactored Role Association Uploads (Beta)</strong>’ option using <strong>Global Configurations &gt; Preferences &gt; Features </strong>to use the newly refactored code for Roles upload and perform role upload using a background job. For the ‘<strong>Enable Refactored Role Association Uploads (Beta)</strong>’ selection to come into effect, you must also select the ‘<strong>Enable Upload Framework (Beta)</strong>’ option mentioned above too. </p>
            </li>
        </ul>
        <h2 id="MyUploads-UploadedReports">Uploaded Reports</h2>
        <p>Once you have enabled the above-mentioned configurations, the bulk-user upload from ARS and Roles upload is done using a background job. The following section describes how you can track the upload status using Uploaded Reports.</p>
        <p>To view the uploaded report, perform the following steps:</p>
        <ol>
            <li>
                <p>Click the ‘<strong>Upload Files</strong>’ icon visible at the top of the navigation panel.</p>
                <p>
                    <img src="../Resources/Images/Upload Files icon.png" />
                </p>
                <p>Figure: Upload Files icon<br />The Uploaded Reports screen is displayed as shown in the following figure.</p>
                <p>
                    <img src="../Resources/Images/Centralized report of uploads.png" style="border-left-style: solid;border-left-width: 1px;border-left-color: ;border-right-style: solid;border-right-width: 1px;border-right-color: ;border-top-style: solid;border-top-width: 1px;border-top-color: ;border-bottom-style: solid;border-bottom-width: 1px;border-bottom-color: ;width: 1200px;height: 500px;" />
                </p>
                <p>Figure: Centralized report of uploads performed<br /></p>
            </li>
            <li>
                <p>Track the upload status using the Upload Status column.<br />The Uploaded Report comprises of the following columns:</p>
                <ol style="list-style-type: lower-alpha;">
                    <li>
                        <p><b>FILEPATH</b>: Indicates the path where the Upload Report zip file is saved.</p>
                    </li>
                    <li>
                        <p><b>USER</b>: Indicates the username who performed the upload for the respective line-item.</p>
                    </li>
                    <li>
                        <p><b>REPORT TYPE</b>: Indicates whether it is a Roles Upload or Request Upload. The Request Upload that is supported for the new upload framework is ‘Bulk-User’ upload request from ARS.</p>
                    </li>
                    <li>
                        <p><b>CREATED ON</b>: Indicates the date and time when the Upload Report file was created.</p>
                    </li>
                    <li>
                        <p><b>DOWNLOADED</b>: Indicates Yes if the file is already downloaded.</p>
                    </li>
                    <li>
                        <p><b>UPLOAD STATUS</b>: The following are the various upload status:</p>
                        <ol style="list-style-type: lower-roman;">
                            <li>
                                <p><b>Pending</b>: A pending status is displayed when the upload is yet to start.</p>
                            </li>
                            <li>
                                <p><b>in-Progress</b>: An In-Progress status is displayed when the upload has started.</p>
                            </li>
                            <li>
                                <p><b>Validation Failed</b>: A Validation-Failed status is displayed when the upload has failed for all the line-items. This status is displayed only for bulk-users upload and not for Roles upload.</p>
                            </li>
                            <li>
                                <p><b>Completed</b>: A Completed status is displayed when the upload process has completed. The Completed status can have data with a successful upload and can have line-items for which validation has failed. For line-items that have an issue, a reason is specified in the Validation Failed column as shown in the following figure.</p>
                                <p>
                                    <img src="../Resources/Images/Completed Status with failed.png" style="border-left-style: solid;border-left-width: 1px;border-left-color: ;border-right-style: solid;border-right-width: 1px;border-right-color: ;border-top-style: solid;border-top-width: 1px;border-top-color: ;border-bottom-style: solid;border-bottom-width: 1px;border-bottom-color: ;width: 1200px;" />
                                </p>
                                <p>Figure: Completed Status with failed validation in line-items</p>
                            </li>
                            <li>
                                <p><b>Failed</b>: A Failed status is displayed when an upload process fails or stops while running the upload.</p>
                            </li>
                        </ol>
                    </li>
                    <li>
                        <p><b>SIZE (KB)</b>: Indicates the size of the uploaded report zip file.</p>
                    </li>
                    <li>
                        <p><b>ACTION</b>: You can perform the following three actions for an Uploaded Report.</p>
                        <ol style="list-style-type: lower-roman;">
                            <li>
                                <p><b>View Summary</b>: Displays a synopsis of roles, users, entitlement, owners related changes done as part of the upload operation.</p>
                            </li>
                            <li>
                                <p><b>Download and Delete</b>: Allows you to download the Upload Report zip file and delete it from the IGA application.</p>
                            </li>
                            <li>
                                <p><b>Delete</b>: Deletes the Upload Report from the IGA application.</p>
                                <div class="note" MadCap:autonum="&lt;b&gt;Note&lt;/b&gt;">
                                    <p class="note">To enable the ‘Download and Delete’ and ‘Delete’ option, you need to run the request map SQL query in the database.</p>
                                </div>
                            </li>
                        </ol>
                    </li>
                </ol>
            </li>
        </ol>
    </body>
</html>