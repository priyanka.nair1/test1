﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head><title>Saviynt SIEM Integration</title>
    </head>
    <body>
        <h1>Saviynt SIEM Integration</h1>
        <h2>Overview</h2>
        <p>SIEM (Security Information and Event Management) is a key part of the overall security framework of&#160;organizations. It is essential to feed the required security information from enterprise applications into SIEM&#160;to monitor and manage security events across the enterprise. Saviynt Identity Governance and Administration (IGA) provides&#160;a specific component to enable&#160;the&#160;integration with SIEM products.</p>
        <p>IGA records all user activities in the form of security audit logs. You can extract these logs from IGA in the CSV file format and transfer the file to the AWS S3 bucket for consumption by a SIEM product such as Splunk or Qradar. On the SIEM side, filters are enabled to search for the required patterns in the file to monitor the user activity.</p>
        <p>The SIEM integration addresses the following use cases:</p>
        <ul>
            <li>
                <p>Monitor security events through the Security Operations team.</p>
            </li>
            <li>
                <p>Ingest IGA Security audit logs to the SIEM tool.</p>
            </li>
            <li>
                <p>Security audit logs provide transactional data of a user’s activity within IGA.</p>
            </li>
        </ul>
        <div class="confluence-information-macro has-no-icon confluence-information-macro-note conf-macro output-block">
            <div class="note" MadCap:autonum="&lt;b&gt;Note&lt;/b&gt;">
                <p class="note">
                    <p>  &#160;The following are not part of the integration:</p>
                    <ul>
                        <li>
                            <p>Use of Syslog format for integration with SIEM products.</p>
                        </li>
                        <li>
                            <p>Ingestion of IGA application logs (debug or error logs) into SIEM products.</p>
                        </li>
                    </ul>
                </p>
            </div>
        </div>
        <h2>Prerequisites</h2>
        <p>The archival policies on the S3 bucket are unique for each environment. Ensure that the following prerequisites are met for your environment:</p>
        <ul>
            <li>
                <p>
                Access is provided to S3 bucket.
            </p>
            </li>
            <li>
                <p>
                Access is provided to IGA.
            </p>
            </li>
            <li>
                <p>
                Azure account (Saviynt’s Infra team provides a 12-digit account ID)
            </p>
            </li>
        </ul>
        <p>Ensure that the following pre-requisites are done in IGA while configuring an Analytics record:</p>
        <ul>
            <li>
                <p>Specify number of analytics history records to keep.</p>
            </li>
            <li>
                <p>Set the Enable Archival to <strong>ON</strong>.</p>
            </li>
        </ul>
        <p>
            <img src="../Resources/Images/SIEM_History_Records.png" style="border-left-style: solid;border-left-width: 1px;border-right-style: solid;border-right-width: 1px;border-top-style: solid;border-top-width: 1px;border-bottom-style: solid;border-bottom-width: 1px;" />
        </p>
        <h3>Reference Architecture
        </h3>
        <p>The architecture diagram provides details about how the event log files are extracted from IGA in the form of SIEM.jar file which is then transported to Amazon S3 where the SIEM tools such as QRadar or Splunk process the file according to the built-in filters.</p>
        <p>
            <img src="../Resources/Images/SA77.png" style="border-left-style: solid;border-left-width: 1px;border-right-style: solid;border-right-width: 1px;border-top-style: solid;border-top-width: 1px;border-bottom-style: solid;border-bottom-width: 1px;" />
        </p>
        <p>Fig: The process of extracting even logs files from IGA to other locations.</p>
        <h2>Security Audit Log Format</h2>
        <p>Security audit log file generated from IGA consists of the following data elements:</p>
        <ul>
            <li>
                <p>Username: Unique ID of the user within IGA.</p>
            </li>
            <li>
                <p>First Name: First Name of the user within IGA.</p>
            </li>
            <li>
                <p>Last name: Last Name of the user within IGA.</p>
            </li>
            <li>
                <p>Access Details: User-readable module name of the URL/Module/Functionality a user has tried to&#160;access within IGA.</p>
            </li>
            <li>
                <p>Access TimeStamp: Timestamp in UTC when the user tried to access the&#160;URL/Module/Functionality within IGA.</p>
            </li>
        </ul>
        <p>IGA maintains a security audit log of the activities performed by a user within the identity warehouse of IGA.&#160;You can obtain these logs in the form of CSV file from IGA using one of the following methods:</p>
        <ul>
            <li>
                <p>File exchange using S3 using the AWS account.</p>
            </li>
            <li>
                <p>SFTP based file transfer without the AWS account.</p>
            </li>
            <li>
                <p>File exchange using Azure Storage Account for Azure tenant.</p>
            </li>
        </ul>
        <h3>File exchange using S3</h3>
        <p>You can get files from AWS S3 bucket by using S3 Copy Operation.&#160;The AWS account access to&#160;S3 bucket needs to be granted for IGA and set up the account by using AWS S3 Bucket Policy.</p>
        <p>Perform the following steps to&#160;get files using S3 Copy Operation:</p>
        <ol>
            <li>
                <p>You need to attach the following bucket policy to S3 bucket from where IGA can get the files.</p>
                <div class="code panel pdl conf-macro output-block" style="border-width: 1px;">
                    <div class="codeContent panelContent pdl">
                        <MadCap:codeSnippet>
                            <MadCap:codeSnippetCopyButton aria-label="Copy" title="Copy to clipboard" />
                            <MadCap:codeSnippetCaption>Sample file</MadCap:codeSnippetCaption>
                            <MadCap:codeSnippetBody MadCap:useLineNumbers="False" MadCap:lineNumberStart="1" MadCap:continue="False" xml:space="preserve" style="padding-left: 12px;padding-right: 16px;padding-top: 12px;padding-bottom: 12px;mc-code-lang: PlainText;">{
"Id": "Policy1488826126935",
"Version": "2012-10-17",
"Statement": [
{
"Sid": "Stmt1488826121402",
"Action": [
"s3:GetBucketLocation",
								
"s3:GetObject",
"s3:ListBucket"],
"Effect": "Allow",
"Resource": [
"arn:aws:s3:::&lt;CustomerS3BucketName&gt;",
"arn:aws:s3:::&lt;CustomerS3BucketName&gt;/*"],
"Principal": {
"AWS": [
"arn:aws:iam::&lt;Saviynt AWS Account 12 Digit ID&gt;:root"]
},
              "Condition": {
         "IpAddress": {"aws:SourceIp": 
                               ["&lt;Saviynt NAT IP ADDRESS1&gt;/32", "&lt;Saviynt NAT IP ADDRESS1&gt;/32"]
                                      }
                             }
}
]
}</MadCap:codeSnippetBody>
                        </MadCap:codeSnippet>
                    </div>
                </div>
                <div class="note" MadCap:autonum="&lt;b&gt;Note&lt;/b&gt;">
                    <ul>
                        <li>
                            <p class="note">IGA requires s3:GetBucketLocation, s3:GetObject, s3:ListBucket permissions to copy the files from the AWS Account associated to S3 bucket.</p>
                        </li>
                        <li>
                            <p class="note">IGA provides the 12-digit account ID for the IGA AWS account.</p>
                        </li>
                    </ul>
                </div>
            </li>
            <li>
                <p class="auto-cursor-target">Attach the following IAM Policy to EC2 IAM Role to delegate access to the bucket in associated AWS account.<br />The S3 bucket name needs to provide in the following Policy.</p>
                <div class="code panel pdl conf-macro output-block" style="border-width: 1px;">
                    <div class="codeContent panelContent pdl">
                        <MadCap:codeSnippet>
                            <MadCap:codeSnippetCopyButton aria-label="Copy" title="Copy to clipboard" />
                            <MadCap:codeSnippetCaption>Sample Policy</MadCap:codeSnippetCaption>
                            <MadCap:codeSnippetBody MadCap:useLineNumbers="False" MadCap:lineNumberStart="1" MadCap:continue="False" xml:space="preserve" style="padding-left: 12px;padding-right: 16px;padding-top: 12px;padding-bottom: 12px;mc-code-lang: PlainText;">{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "Stmt1357935647218",
            "Effect": "Allow",
            "Action": [
                "s3:GetBucketLocation",
                "s3:ListBucket"            ],
            "Resource": "arn:aws:s3:::&lt; CustomerS3BucketName &gt;"        },
        {
            "Sid": "Stmt1357935676138",
            "Effect": "Allow",
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::&lt; CustomerS3BucketName/*&gt;"        }
    ]
}</MadCap:codeSnippetBody>
                        </MadCap:codeSnippet>
                    </div>
                </div>
            </li>
            <li>
                <p>Once the permissions are set up in IGA and AWS account, the following command can be used to get the files.</p>
                <div class="note" MadCap:autonum="&lt;b&gt;Note&lt;/b&gt;">
                    <p class="note">
                        <p>The default AWS CLI commands are sent to SSL channel:&#160; <a href="https://docs.aws.amazon.com/cli/latest/reference/"><a href="http://docs.aws.amazon.com/cli/latest/reference/" class="external-link" rel="nofollow">http://docs.aws.amazon.com/cli/latest/reference/</a></a></p>
                    </p>
                </div>
                <p>Use the following command during a put or copy operation. The object owner can specify that the ACL of the object gives full control to the bucket owner.</p>
                <MadCap:codeSnippet>
                    <MadCap:codeSnippetCopyButton aria-label="Copy" title="Copy to clipboard" />
                    <MadCap:codeSnippetCaption>Command for Put or Copy Operation</MadCap:codeSnippetCaption>
                    <MadCap:codeSnippetBody MadCap:useLineNumbers="False" MadCap:lineNumberStart="1" MadCap:continue="False" xml:space="preserve" style="padding-left: 12px;padding-right: 16px;padding-top: 12px;padding-bottom: 12px;mc-code-lang: PlainText;">aws s3 cp &lt;yourfilenamewithlocation&gt;  s3://&lt;CustomersAWSS3bucketname&gt;/ --ACL bucket-owner-full-control --recursive</MadCap:codeSnippetBody>
                </MadCap:codeSnippet>
                <p>Remove the recursive option if you are not copying multiple objects.</p>
            </li>
            <li>
                <p>Prepare a shell script to mention the copy command listed above and schedule it using the cron expression.</p>
                <MadCap:codeSnippet>
                    <MadCap:codeSnippetCopyButton aria-label="Copy" title="Copy to clipboard" />
                    <MadCap:codeSnippetCaption>Shell script:</MadCap:codeSnippetCaption>
                    <MadCap:codeSnippetBody MadCap:useLineNumbers="False" MadCap:lineNumberStart="1" MadCap:continue="False" xml:space="preserve" style="padding-left: 12px;padding-right: 16px;padding-top: 12px;padding-bottom: 12px;mc-code-lang: PlainText;">#!/bin/sh
# [Description] This script is created end the the generated audit log into customer's AWS S3 bucket location.# 
# Options processing
date=$(TZ=Asia/Kuala_Lumpur date "+%Y%m%d%H%M%S")
logfile=/opt/sharedappdrive/saviynt/logs/AuditLog_$date.log
echo "********START OF LOG********"&gt;&gt; $logfile
cd /opt/sharedappdrive/saviynt/reports
inputfile=`ls -t Analytics_Summary_AuditLogReport* | head -1`
outputfile=SIEM-AuditLogReport_$date.csv
echo "The input file is $inputfile" &gt;&gt; $logfile
echo "The Output file is $outputfile" &gt;&gt; $logfile
python /opt/sharedappdrive/saviynt/Scripts/xlsx2csv.py --output /opt/sharedappdrive/saviynt/output/$outputfile /opt/sharedappdrive/saviynt/reports/$inputfile &gt;&gt; $logfile
echo "After executing the Python command" &gt;&gt; $logfile
aws s3 cp &lt;yourfilenamewithlocation&gt;  s3://&lt;CustomersAWSS3bucketname&gt;/ --ACL bucket-owner-full-control
echo "Audit log report copied to customer's S3 bucket location" &gt;&gt; $logfile</MadCap:codeSnippetBody>
                </MadCap:codeSnippet>
                <MadCap:codeSnippet>
                    <MadCap:codeSnippetCopyButton aria-label="Copy" title="Copy to clipboard" />
                    <MadCap:codeSnippetCaption>cron expression:</MadCap:codeSnippetCaption>
                    <MadCap:codeSnippetBody MadCap:useLineNumbers="False" MadCap:lineNumberStart="1" MadCap:continue="False" xml:space="preserve" style="padding-left: 12px;padding-right: 16px;padding-top: 12px;padding-bottom: 12px;mc-code-lang: PlainText;">0 20 * * * /opt/sharedappdrive/saviynt/Scripts/SIEMAuditLog.sh</MadCap:codeSnippetBody>
                </MadCap:codeSnippet>
            </li>
        </ol>
        <h3 id="SaviyntSIEMIntegration-SFTPBasedFileTransfer(ForbothFilePush/Pull)">SFTP Based File Transfer (For both File Push/Pull)
        </h3>
        <ol>
            <li>
                <p>The SFTP location and credentials can be used to connect to the SFTP Server which is used to push/pull files as per requirement.</p>
            </li>
            <li>
                <p>The credentials are stored in the AWS parameter of IGA and&#160;a separate command is used as a string for SFTP Password.</p>
            </li>
            <li>
                <p>The plain text cannot be used for scripts or text files on the server. Such files need to be removed in accordance with security violations</p>
                <MadCap:codeSnippet>
                    <MadCap:codeSnippetCopyButton aria-label="Copy" title="Copy to clipboard" />
                    <MadCap:codeSnippetCaption>Sample SFTP</MadCap:codeSnippetCaption>
                    <MadCap:codeSnippetBody MadCap:useLineNumbers="False" MadCap:lineNumberStart="1" MadCap:continue="False" xml:space="preserve" style="padding-left: 12px;padding-right: 16px;padding-top: 12px;padding-bottom: 12px;mc-code-lang: PlainText;">#!/bin/bash
export SSHPASS=`aws ssm get-parameters --names 
"customer1-target-sftppass" --with-decryption --query 
"Parameters[*]["Value"]" --output text --region us-east-1`
sshpass -e sftp -oBatchMode=no -b – user@server &lt;&lt; !
lcd /opt/saviynt/Import/
 get Prod/Out/SCHEMA*
 bye
!
echo "File copied"</MadCap:codeSnippetBody>
                </MadCap:codeSnippet>
            </li>
        </ol>
        <h3 id="SaviyntSIEMIntegration-FileexchangeusingAzureStorageAccount">File exchange using Azure Storage Account
        </h3>
        <ol>
            <li>
                <p>Create Azure Storage in the storage account as shown in the following figure:<br /><img src="../Resources/Images/SA80.png" style="border-left-style: solid;border-left-width: 1px;border-right-style: solid;border-right-width: 1px;border-top-style: solid;border-top-width: 1px;border-bottom-style: solid;border-bottom-width: 1px;" /><br />Fig: Storage account window after storage creation</p>
            </li>
            <li>
                <p>Click on File Share and create a container.</p>
            </li>
            <li>
                <p>Share the details of a file container URL and Key1 or Key2 as shown in the following figure:<br /><img src="../Resources/Images/SA81.png" style="border-left-style: solid;border-left-width: 1px;border-right-style: solid;border-right-width: 1px;border-top-style: solid;border-top-width: 1px;border-bottom-style: solid;border-bottom-width: 1px;" /><br />Fig: File properties window<br /><img src="../Resources/Images/SA82.png" style="border-left-style: solid;border-left-width: 1px;border-right-style: solid;border-right-width: 1px;border-top-style: solid;border-top-width: 1px;border-bottom-style: solid;border-bottom-width: 1px;" /><br />Fig: Access log window</p>
            </li>
            <li>
                <p>Access to blob/fileshare container:<br />Need access to the Fileshare Storage/ Blob Storage by getting details of key and URL of the storage account along with whitelisting of IP if the storage account is at IGA or other locations.</p>
                <p>
                    <img src="../Resources/Images/SA84.png" style="border-left-style: solid;border-left-width: 1px;border-right-style: solid;border-right-width: 1px;border-top-style: solid;border-top-width: 1px;border-bottom-style: solid;border-bottom-width: 1px;" />
                    <br />Fig: Network and firewall window</p>
            </li>
            <li>
                <p>AZ Copy for push and pull the file from Azure Storage Account.<br />IGA is using the Azcopy PowerShell module on the Linux server to pull and push the data from the Azure storage account. The AzCopy binary&#160;on Linux Azcopy executables needs to install on windows.<ul><li><p><strong>To Push Single file:</strong><br />The following command pushes a file name to the storage container in the container.<br /></p><MadCap:codeSnippet><MadCap:codeSnippetCopyButton aria-label="Copy" title="Copy to clipboard" /><MadCap:codeSnippetCaption>Command to Push Single File</MadCap:codeSnippetCaption><MadCap:codeSnippetBody MadCap:useLineNumbers="False" MadCap:lineNumberStart="1" MadCap:continue="False" xml:space="preserve" style="padding-left: 12px;padding-right: 16px;padding-top: 12px;padding-bottom: 12px;mc-code-lang: PlainText;">azcopy --source &lt;complete-source-file-path&gt; --destination &lt;blobstorage url&gt; --dest-key &lt;destkey&gt;</MadCap:codeSnippetBody></MadCap:codeSnippet></li><li><p><strong>To Push Files in a folder recursively:</strong><br />The following command pushes all files in the target reports folder to the storage container.<br /></p><MadCap:codeSnippet><MadCap:codeSnippetCopyButton aria-label="Copy" title="Copy to clipboard" /><MadCap:codeSnippetCaption>Command to Push Files in a Folder Recursively</MadCap:codeSnippetCaption><MadCap:codeSnippetBody MadCap:useLineNumbers="False" MadCap:lineNumberStart="1" MadCap:continue="False" xml:space="preserve" style="padding-left: 12px;padding-right: 16px;padding-top: 12px;padding-bottom: 12px;mc-code-lang: PlainText;">azcopy --source &lt;source-folder-path&gt; --destination &lt;destination-blob-storage&gt; --dest-key &lt;destkey&gt; --recursive</MadCap:codeSnippetBody></MadCap:codeSnippet></li><li><p class="auto-cursor-target"><strong>To Pull a Single File:</strong><br />The following command&#160;pull a file on the storage container to the local folder &lt; path to local folder &gt;</p><MadCap:codeSnippet><MadCap:codeSnippetCopyButton aria-label="Copy" title="Copy to clipboard" /><MadCap:codeSnippetCaption>Command to Pull a Single File</MadCap:codeSnippetCaption><MadCap:codeSnippetBody MadCap:useLineNumbers="False" MadCap:lineNumberStart="1" MadCap:continue="False" xml:space="preserve" style="padding-left: 12px;padding-right: 16px;padding-top: 12px;padding-bottom: 12px;mc-code-lang: PlainText;">azcopy --source &lt;sourcefilestorageaccount&gt; --destination &lt;destinationfilepath&gt; --source-key &lt; storageaccountkey &gt;</MadCap:codeSnippetBody></MadCap:codeSnippet></li><li><p class="auto-cursor-target"><strong>To Pull multiple files recursively:</strong><br />The following command&#160;pull all files from a storage container to a local folder &lt; path to local folder &gt;</p><MadCap:codeSnippet><MadCap:codeSnippetCopyButton aria-label="Copy" title="Copy to clipboard" /><MadCap:codeSnippetCaption>Command to Pull Multiple Files Recursively</MadCap:codeSnippetCaption><MadCap:codeSnippetBody MadCap:useLineNumbers="False" MadCap:lineNumberStart="1" MadCap:continue="False" xml:space="preserve" style="padding-left: 12px;padding-right: 16px;padding-top: 12px;padding-bottom: 12px;mc-code-lang: PlainText;">azcopy --source &lt; sourcefilestorageaccount &gt; --destination &lt; destinationfilepath &gt; --source-key &lt;storageaccountkey&gt;  --recursive  </MadCap:codeSnippetBody></MadCap:codeSnippet></li></ul></p>
            </li>
        </ol>
    </body>
</html>