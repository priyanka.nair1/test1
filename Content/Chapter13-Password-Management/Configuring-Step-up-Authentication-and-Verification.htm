﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head>
        <link href="../Resources/TableStyles/Default-Table.css" rel="stylesheet" MadCap:stylesheetType="table" /><title>Configuring Step-up Authentication and Verification</title>
        <link href="../Resources/Stylesheets/Styles.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <h1>Configuring Step-up Authentication and Verification</h1>
        <h2>Introduction</h2>
        <p>Step-Up authentication is a multi-step authentication method in which users are asked to produce additional forms of authentication to verify who they claim to be. Typically, it includes two or more levels of authentication. Step-up authentication is useful for organizations where users must verify themselves with more than one mode of authentication to perform high-risk actions.</p>
        <p>Step-Up authentication supports the following authentication methods:</p>
        <ul>
            <li>
                <p>Knowledge-Based Authentication (KBA)</p>
            </li>
            <li>
                <p>One Time Password through SMS or email (OTP)</p>
                <ul>
                    <li>
                        <p><strong>SMS OTP:</strong> In this method, an OTP is sent with the SMS text to the user’s phone. The user receives the OTP and enters it on the device where the authentication is happening. The OTP must be used within a specific time frame. The OTPs delivered through text messages prevent phishing and malicious attacks.</p>
                    </li>
                    <li>
                        <p><strong>Email OTP:</strong> In this method, an email is sent with an OTP to the user's e-mail address. The user must specify the OTP on the device where the user needs to authenticate. It is a good practice to use the Email OTP authentication method with the typical user name/password authentication method to achieve step-up authentication and to prohibit malicious users from sending SPAM mails to a user's email box with authentication requests.</p>
                    </li>
                </ul>
            </li>
        </ul>
        <p>Sending OTP through SMS or email requires you to perform additional configuration. For more information, see <MadCap:xref href="#Configur">Configuring OTP Authentication Methods</MadCap:xref>.</p>
        <p>Currently, Step-Up authentication is available only for the Password Management functionality and it can be used with the following operations:</p>
        <ul>
            <li>
                <p>Changing an account password</p>
            </li>
            <li>
                <p>Resetting the account password for others</p>
            </li>
            <li>
                <p>Resetting user password</p>
            </li>
            <li>
                <p>Changing user password from the Profile Menu</p>
            </li>
            <li>
                <p>Resetting a password in case of forgotten password</p>
            </li>
            <li>
                <p>Resetting security questions</p>
            </li>
        </ul>
        <div class="note" MadCap:autonum="&lt;b&gt;Note&lt;/b&gt;">
            <ul>
                <li>
                    <p class="note">Users are prompted to verify their identity only if Step-Up authentication is configured by the administrator.</p>
                </li>
                <li>
                    <p class="note">For the <strong>Forgot Password</strong> action, the default method of verification is KBA (security question and answer-based authentication) if Step-Up authentication is not configured. End-users will need to verify themselves using pre-configured security questions. If no security questions are present, end-users need to reach out to the administrator or helpdesk for assistance. For other password-related actions, there’s no default verification method if Step-Up authentication is not configured. End-users can use those actions without any authentication or re-verification.</p>
                </li>
                <li>
                    <p class="note">For <strong>Reset Account Password for Others</strong> and <b>Reset User Password</b> actions, both the logged-in user and the beneficiary user can be configured for verification.</p>
                </li>
            </ul>
        </div>
        <h2 id="ConfiguringStep-upAuthenticationandVerification-ConfiguringStep-UpAuthentication">Configuring Step-Up Authentication</h2>
        <ol>
            <li>
                <p>Go to <strong>Admin &gt; Global Configurations &gt; Step-up Authentication</strong>. The <strong>STEP-UP AUTHENTICATION/VERIFICATION MAPPING</strong> section specifies the mapping of the authentication or verification methods. The <strong>OTP CONFIGURATIONS</strong> section specifies the configurations needed for enabling SMS and Email based OTPs.</p>
            </li>
            <li>
                <p>For all actions listed under the <strong>PASSWORD MANAGEMENT</strong> section, select the options (SMS, Email, KBA) and fill in values for the given configurations.</p>
            </li>
        </ol>
        <p>
            <img src="../Resources/Images/globalconfig.png" style="width: 650px;" />
        </p>
        <div class="table-wrap">
            <table class="TableStyle-Default-Table" style="width: 100%;mc-table-style: url('../Resources/TableStyles/Default-Table.css');" cellspacing="0">
                <col style="width: 340px;" class="TableStyle-Default-Table-Column-Column1" />
                <col style="width: 340px;" class="TableStyle-Default-Table-Column-Column1" />
                <thead>
                    <tr class="TableStyle-Default-Table-Head-Header1">
                        <th class="TableStyle-Default-Table-HeadE-Column1-Header1">
                            <p class="table-text">Option</p>
                        </th>
                        <th class="TableStyle-Default-Table-HeadD-Column1-Header1">
                            <p class="table-text">Description</p>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="TableStyle-Default-Table-Body-Body1">
                        <td class="TableStyle-Default-Table-BodyE-Column1-Body1">
                            <p class="table-text">Change Account Password for Self</p>
                        </td>
                        <td class="TableStyle-Default-Table-BodyD-Column1-Body1">
                            <p class="table-text">When users change passwords of their accounts for different endpoints.</p>
                        </td>
                    </tr>
                    <tr class="TableStyle-Default-Table-Body-Body1">
                        <td class="TableStyle-Default-Table-BodyE-Column1-Body1">
                            <p class="table-text">Reset Account Password for Others - Step Up</p>
                        </td>
                        <td class="TableStyle-Default-Table-BodyD-Column1-Body1">
                            <p class="table-text">When an admin, helpdesk, or manager changes account passwords for other users, the configured method is used as an additional authentication method for verification.</p>
                        </td>
                    </tr>
                    <tr class="TableStyle-Default-Table-Body-Body1">
                        <td class="TableStyle-Default-Table-BodyE-Column1-Body1">
                            <p class="table-text">Reset Account Password for Others - Verify Beneficiary</p>
                        </td>
                        <td class="TableStyle-Default-Table-BodyD-Column1-Body1">
                            <p class="table-text">When an admin, helpdesk, or manager resets the account password for other users, the configured method is used as an additional authentication method for verifying the identity of the beneficiary user.</p>
                        </td>
                    </tr>
                    <tr class="TableStyle-Default-Table-Body-Body1">
                        <td class="TableStyle-Default-Table-BodyE-Column1-Body1">
                            <p class="table-text">Change Password</p>
                        </td>
                        <td class="TableStyle-Default-Table-BodyD-Column1-Body1">
                            <p class="table-text">Additional authentication for a user when the user initiates a password change from the Profile menu.</p>
                        </td>
                    </tr>
                    <tr class="TableStyle-Default-Table-Body-Body1">
                        <td class="TableStyle-Default-Table-BodyE-Column1-Body1">
                            <p class="table-text">Forgot Password</p>
                        </td>
                        <td class="TableStyle-Default-Table-BodyD-Column1-Body1">
                            <p class="table-text">Additional verification for a user when the user initiates a password reset in case of a forgotten password.</p>
                        </td>
                    </tr>
                    <tr class="TableStyle-Default-Table-Body-Body1">
                        <td class="TableStyle-Default-Table-BodyE-Column1-Body1">
                            <p class="table-text">Reset User Password for Others - Verify Beneficiary</p>
                        </td>
                        <td class="TableStyle-Default-Table-BodyD-Column1-Body1">
                            <p class="table-text">When admin, helpdesk, or manager resets the password for other users, the configured method is used as an additional authentication method for verifying the identity of the beneficiary user.</p>
                        </td>
                    </tr>
                    <tr class="TableStyle-Default-Table-Body-Body1">
                        <td class="TableStyle-Default-Table-BodyE-Column1-Body1">
                            <p class="table-text">Reset User Password for Others - Step Up</p>
                        </td>
                        <td class="TableStyle-Default-Table-BodyD-Column1-Body1">
                            <p class="table-text">Additional authentication for verifying a user who is logged in to the system when admin, helpdesk, or manager is in the process of resetting that user’s password.</p>
                        </td>
                    </tr>
                    <tr class="TableStyle-Default-Table-Body-Body1">
                        <td class="TableStyle-Default-Table-BodyB-Column1-Body1">
                            <p class="table-text">Reset Security Questions</p>
                        </td>
                        <td class="TableStyle-Default-Table-BodyA-Column1-Body1">
                            <p class="table-text">Additional verification to verify a user when a user initiates the resetting of security questions.</p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <h2 id="ConfiguringStep-upAuthenticationandVerification-ConfiguringOTPAuthenticationMethods"><a name="Configur"></a>Configuring OTP Authentication Methods</h2>
        <p>You can configure the OTP-based authentication by using the SMS OTP or the email OTP method. </p>
        <h3 id="ConfiguringStep-upAuthenticationandVerification-UsingSaviyntSecurityManagerastheOTPProvider">Using Saviynt Security Manager as the OTP Provider</h3>
        <p>To use Saviynt Identity Governance and Administration (IGA) for generating OTPs for SMS and email methods:</p>
        <ol>
            <li>
                <p>Go to <strong>Admin &gt; Global Configurations &gt; Step-up Authentication</strong>.</p>
            </li>
            <li>
                <p>Under the <strong>OTP CONFIGURATIONS</strong> section, select <strong>Do you want Saviynt to generate OTP?</strong> and fill in values for the following options:<img src="../Resources/Images/otpconfig.png" style="width: 650px;" /></p>
            </li>
        </ol>
        <div class="table-wrap">
            <table class="TableStyle-Default-Table" style="width: 100%;mc-table-style: url('../Resources/TableStyles/Default-Table.css');" cellspacing="0">
                <col style="width: 243.0px;" class="TableStyle-Default-Table-Column-Column1" />
                <col style="width: 516.0px;" class="TableStyle-Default-Table-Column-Column1" />
                <thead>
                    <tr class="TableStyle-Default-Table-Head-Header1">
                        <th class="TableStyle-Default-Table-HeadE-Column1-Header1">
                            <p class="table-text">Option </p>
                        </th>
                        <th class="TableStyle-Default-Table-HeadD-Column1-Header1">
                            <p class="table-text">Description</p>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="TableStyle-Default-Table-Body-Body1">
                        <td class="TableStyle-Default-Table-BodyE-Column1-Body1">
                            <p class="table-text">OTP Provider Name</p>
                        </td>
                        <td class="TableStyle-Default-Table-BodyD-Column1-Body1">
                            <p class="table-text">Select the name of the REST connector that you configured for generating, managing, and sending OTPs .</p>
                            <div class="note" MadCap:autonum="&lt;b&gt;Note&lt;/b&gt;">
                                <p class="note">This field is applicable for SMS authentication. </p>
                            </div>
                        </td>
                    </tr>
                    <tr class="TableStyle-Default-Table-Body-Body1">
                        <td class="TableStyle-Default-Table-BodyE-Column1-Body1">
                            <p class="table-text">OTP Token Length</p>
                        </td>
                        <td class="TableStyle-Default-Table-BodyD-Column1-Body1">
                            <p class="table-text">Enter the length of the OTP token. The default value is 6.</p>
                        </td>
                    </tr>
                    <tr class="TableStyle-Default-Table-Body-Body1">
                        <td class="TableStyle-Default-Table-BodyE-Column1-Body1">
                            <p class="table-text">OTP Token Validity</p>
                        </td>
                        <td class="TableStyle-Default-Table-BodyD-Column1-Body1">
                            <p class="table-text">Enter the lifetime of the OTP in seconds. </p>
                            <div class="note" MadCap:autonum="&lt;b&gt;Note&lt;/b&gt;">
                                <p class="note">The default value is 300 seconds. There is no maximum or minimum allowed value restriction.</p>
                            </div>
                        </td>
                    </tr>
                    <tr class="TableStyle-Default-Table-Body-Body1">
                        <td class="TableStyle-Default-Table-BodyB-Column1-Body1">
                            <p class="table-text">OTP Delivery Template (Email)</p>
                        </td>
                        <td class="TableStyle-Default-Table-BodyA-Column1-Body1">
                            <p class="table-text">Select the email template for sending the OTP to end-users. </p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <h3 id="ConfiguringStep-upAuthenticationandVerification-SMSOTP">SMS OTP</h3>
        <p>For using SMS OTP, create a REST connector and define the authentication type supported by the SMS gateway service in the <strong>ConnectionJSON</strong> parameter of the connector to connect with its REST web service. To deliver the SMS to end users, define the <strong>SendOTPJSON </strong>parameter. Each SMS gateway uses a different proprietary communication protocol that acts as a relay to translate one protocol into another. The URI of the REST connector will be the URI of the SMS gateway service. </p>
        <div class="note" MadCap:autonum="&lt;b&gt;Note&lt;/b&gt;">
            <p class="note">Saviynt does not provide an SMS gateway. To use IGA as the OTP Provider, customers must configure the SMS gateway service (supported API) with a third-party service provider.</p>
        </div>
        <p>To configure the SMS OTP method, perform the following actions:</p>
        <ol>
            <li>
                <p>Create a REST connector. </p>
                <p MadCap:conditions="Default.v2020X">For more information, see the <MadCap:conditionalText MadCap:conditions="Default.Link"><a href="REST-Connector-Guide.htm" pubname="REST-v2020x" class="peer">REST Connector Guide</a></MadCap:conditionalText>.</p>
                <p MadCap:conditions="Default.v2021X">For more information, see the <MadCap:conditionalText MadCap:conditions="Default.Link"><a href="REST-Connector-Guide.htm" pubname="REST-v2021x" class="peer">REST Connector Guide</a></MadCap:conditionalText>.</p>
            </li>
            <li>
                <p>Define the following parameters for the connector:</p>
            </li>
        </ol>
        <div class="table-wrap">
            <table class="confluenceTable">
                <colgroup>
                    <col style="width: 175.0px;" />
                    <col style="width: 233.0px;" />
                    <col style="width: 351.0px;" />
                </colgroup>
                <tbody>
                    <tr>
                        <th class="confluenceTh">
                            <p>REST Connector <span class="inline-comment-marker">Parameter</span></p>
                        </th>
                        <th class="confluenceTh">
                            <p><strong>Description</strong>
                            </p>
                        </th>
                        <th class="confluenceTh">
                            <p><strong><span class="inline-comment-marker">Sample Value</span></strong>
                            </p>
                        </th>
                    </tr>
                    <tr>
                        <td class="confluenceTd">
                            <p><strong>ConnectionJSON</strong>
                            </p>
                        </td>
                        <td class="confluenceTd">
                            <p>Specify this parameter to connect to the SMS Gateway’s REST web services using the authentication supported by it.</p>
                        </td>
                        <td class="confluenceTd">
                            <MadCap:codeSnippet>
                                <MadCap:codeSnippetCopyButton aria-label="Copy" title="Copy to clipboard" />
                                <MadCap:codeSnippetCaption>JSON</MadCap:codeSnippetCaption>
                                <MadCap:codeSnippetBody MadCap:useLineNumbers="False" MadCap:lineNumberStart="1" MadCap:continue="False" xml:space="preserve" style="padding-left: 12px;padding-right: 16px;padding-top: 12px;padding-bottom: 12px;mc-code-lang: PlainText;">{
  "authentications": {
    "acctAuth": {
      "authType": "oauth2",
      "httpHeaders": {
        "contentType": "application/json"
      },
      "authError": [
        "InvalidAuthenticationToken",
        "AuthenticationFailed"
      ],
      "url": " https://demo.sms-gateway.com/api/v1 ",
      "httpMethod": "POST",
      "httpContentType": "application/json",
      "errorPath": "error.code",
      "maxRefreshTryCount": 5,
      "tokenResponsePath": "access_token",
      "tokenType": "SSWS",
      "authHeaderName": "Authorization",
      "accessToken": "SSWS &lt;token&gt;",
      "httpParams": "[object Object]",
      "retryFailureStatusCode": []
    }
  }
}    </MadCap:codeSnippetBody>
                            </MadCap:codeSnippet>
                        </td>
                    </tr>
                    <tr>
                        <td class="confluenceTd">
                            <p><strong>SendOtpJSON</strong>
                            </p>
                        </td>
                        <td class="confluenceTd">
                            <p>Specify this parameter to provide the connection details to the SMS gateway service that will send the SMS to the end user’s phone.</p>
                            <ul>
                                <li>
                                    <p>Add the content that you intend to send via SMS as part of this parameter.</p>
                                </li>
                                <li>
                                    <p>Use the following binding variables (mandatory):</p>
                                    <ul>
                                        <li>
                                            <p><strong>${otp} </strong>to obtain the OTP value.</p>
                                        </li>
                                        <li>
                                            <p><strong>${phone} </strong>to obtain the phone number selected by the end-user during the verification process.</p>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </td>
                        <td class="confluenceTd">
                            <MadCap:codeSnippet>
                                <MadCap:codeSnippetCopyButton aria-label="Copy" title="Copy to clipboard" />
                                <MadCap:codeSnippetCaption>JSON</MadCap:codeSnippetCaption>
                                <MadCap:codeSnippetBody MadCap:useLineNumbers="False" MadCap:lineNumberStart="1" MadCap:continue="False" xml:space="preserve" style="padding-left: 12px;padding-right: 16px;padding-top: 12px;padding-bottom: 12px;mc-code-lang: PlainText;">{
  "call": [
    {
      "name": "call1",
      "connection": "acctAuth",
      "url": "https://demo.sms-gateway.com/send",
      "httpMethod": "POST",
      "httpHeaders": {
        "Accept": "application/json",
        "Content-Type": "application/json"
      },
      "httpContentType": "application/json",
      "httpParams": {
        "senderid": "Saviynt-OTP-Admin",
        "message": "Your Saviynt verification code is ${otp}. It will be valid for the next 300 seconds.",
        "phone": "${phone}"
      }
    }
  ]
}    </MadCap:codeSnippetBody>
                            </MadCap:codeSnippet>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <p>Select the configured REST connector for generating, managing, and sending OTP in the <strong>OTP Provider Name</strong> configuration.</p>
        <h3 id="ConfiguringStep-upAuthenticationandVerification-EmailOTP">Email OTP</h3>
        <p>To configure the Email OTP method, perform the following actions:</p>
        <div class="note" MadCap:autonum="&lt;b&gt;Note&lt;/b&gt;">
            <p class="note">Saviynt does not provide an Email gateway. To use Email as the OTP Provider, customers must configure the Email gateway service with a third-party service provider.</p>
        </div>
        <ol>
            <li>
                <p>Configure the SMTP settings for sending emails. For more information, see  SMTP Settings.</p>
            </li>
            <li>
                <p>Define the email template from <strong>Admin &gt; Configurations &gt; Email Templates &gt; Create New Email Template</strong>.</p>
            </li>
        </ol>
        <div class="table-wrap">
            <table class="confluenceTable">
                <colgroup>
                    <col style="width: 155.0px;" />
                    <col style="width: 200.0px;" />
                    <col style="width: 404.0px;" />
                </colgroup>
                <tbody>
                    <tr>
                        <th class="confluenceTh">
                            <p><strong>Email Parameters</strong>
                            </p>
                        </th>
                        <th class="confluenceTh">
                            <p><strong>Description</strong>
                            </p>
                        </th>
                        <th class="confluenceTh">
                            <p><strong>Sample Template</strong>
                            </p>
                        </th>
                    </tr>
                    <tr>
                        <td class="confluenceTd">
                            <p><strong>${otp} </strong>
                            </p>
                            <p><strong>${email}</strong>
                            </p>
                        </td>
                        <td class="confluenceTd">
                            <p>Use this parameter for sending the OTP.</p>
                            <p>Use this parameter for sending the OTP to the user’s email ID.</p>
                        </td>
                        <td class="confluenceTd">
                            <img src="../Resources/Images/emailotp.png" style="width: 550px;" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="important">
            <p class="note">The OTP Delivery Template (Email) is the email template used for delivering the OTPs via email.</p>
        </div>
        <h3 id="ConfiguringStep-upAuthenticationandVerification-UsinganExternalOTPProvider">Using an External OTP Provider</h3>
        <p>You can use an external provider to generate, manage, and send SMS and Email OTPs to end users. To configure an external OTP provider, ensure that the <span class="inline-comment-marker">following setting</span>s are made in the&#160;<strong>OTP CONFIGURATIONS</strong>&#160;section of Global Configurations:</p>
        <ol>
            <li>
                <p><strong>Do you want Saviynt to generate OTP?</strong>&#160;is deselected.</p>
            </li>
            <li>
                <p>OTP parameters such as <strong>OTP Token Length</strong>,<strong> OTP Token Validity</strong>, and<strong> OTP Delivery Template (Email)</strong> are not configured.</p>
            </li>
        </ol>
        <p>In addition, perform the following actions:</p>
        <ul>
            <li>
                <p>Define specific parameters of the REST connector to connect to the external OTP provider service.</p>
            </li>
            <li>
                <p>Issue APIs of the external OTP provider to send email or SMS respectively.</p>
            </li>
            <li>
                <p>Enter the URL, the URI to connect to the external OTP provider’s REST services.</p>
            </li>
        </ul>
        <h4 id="ConfiguringStep-upAuthenticationandVerification-EmailOTP.1"><span class="inline-comment-marker">Email OTP</span>
        </h4>
        <p>End users must have a registered email address in the external OTP provider’s datastore.</p>
        <h4 id="ConfiguringStep-upAuthenticationandVerification-SMSOTP.1"><span class="inline-comment-marker">SMS OTP</span>
        </h4>
        <p>End users must have a registered email address in the external OTP provider’s datastore.</p>
        <div class="table-wrap">
            <table class="confluenceTable">
                <colgroup>
                    <col style="width: 167.0px;" />
                    <col style="width: 283.0px;" />
                    <col style="width: 310.0px;" />
                </colgroup>
                <tbody>
                    <tr>
                        <th class="confluenceTh">
                            <p><strong>REST Connector Parameter</strong>
                            </p>
                        </th>
                        <th class="confluenceTh">
                            <p><strong>Description</strong>
                            </p>
                        </th>
                        <th class="confluenceTh">
                            <p><strong><span class="inline-comment-marker">Sample </span>Value</strong>
                            </p>
                        </th>
                    </tr>
                    <tr>
                        <td class="confluenceTd">
                            <p><strong>ConnectionJSON</strong>
                            </p>
                        </td>
                        <td class="confluenceTd">
                            <p>Specify this parameter to connect to the external OTP provider’s REST web services using the authentication supported by the external OTP provider. </p>
                        </td>
                        <td class="confluenceTd">
                            <MadCap:codeSnippet>
                                <MadCap:codeSnippetCopyButton aria-label="Copy" title="Copy to clipboard" />
                                <MadCap:codeSnippetCaption>JSON</MadCap:codeSnippetCaption>
                                <MadCap:codeSnippetBody MadCap:useLineNumbers="False" MadCap:lineNumberStart="1" MadCap:continue="False" xml:space="preserve" style="padding-left: 12px;padding-right: 16px;padding-top: 12px;padding-bottom: 12px;mc-code-lang: PlainText;">{
  "authentications": {
    "acctAuth": {
      "authType": "oauth2",
      "httpHeaders": {
        "contentType": "application/json"
      },
      "authError": [
        "InvalidAuthenticationToken",
        "AuthenticationFailed"
      ],
      "url": "https://external.otp-provider.com/api/v1",
      "httpMethod": "POST",
      "httpContentType": "application/json",
      "errorPath": "error.code",
      "maxRefreshTryCount": 5,
      "tokenResponsePath": "access_token",
      "tokenType": "SSWS",
      "authHeaderName": "Authorization",
      "accessToken": "SSWS &lt;token&gt;",
      "httpParams": "[object Object]",
      "retryFailureStatusCode": []
    }
  }
}  </MadCap:codeSnippetBody>
                            </MadCap:codeSnippet>
                        </td>
                    </tr>
                    <tr>
                        <td class="confluenceTd">
                            <p><strong>SendOtpJSON</strong>
                            </p>
                        </td>
                        <td class="confluenceTd">
                            <p>Specify this parameter to send an email or SMS-based OTP generated by the external OTP provider to end users. </p>
                            <p>The binding variables to use are:</p>
                            <ul>
                                <li>
                                    <p><strong>${otpSelName}</strong>: Determines if the end-user has chosen an email or SMS based OTP.</p>
                                </li>
                                <li>
                                    <p><strong>${user.username}</strong>,&#160;<strong>${user.firstname}</strong>&#160;and so on: Fetches the end user’s profile properties.</p>
                                    <div class="info" MadCap:autonum="&lt;b&gt;Info&lt;/b&gt;">
                                        <p class="note" MadCap:conditions="Default.v55X">Available from Release v5.5SP2 onwards. </p>
                                    </div>
                                </li>
                            </ul>
                        </td>
                        <td class="confluenceTd">
                            <MadCap:codeSnippet>
                                <MadCap:codeSnippetCopyButton aria-label="Copy" title="Copy to clipboard" />
                                <MadCap:codeSnippetCaption>JSON</MadCap:codeSnippetCaption>
                                <MadCap:codeSnippetBody MadCap:useLineNumbers="False" MadCap:lineNumberStart="1" MadCap:continue="False" xml:space="preserve" style="padding-left: 12px;padding-right: 16px;padding-top: 12px;padding-bottom: 12px;mc-code-lang: PlainText;">{
  "call": [
    {
      "name": "call1",
      "connection": "acctAuth",
      "url": "${otpSelName.equals('email')?'https://external.otp-provider.com/api/v1/users/${user.customproperty10}/verify': 'https://external.otp-provider.com /api/v1/users//${user.customproperty11}/ verify'}",
      "httpMethod": "POST",
      "httpHeaders": {
        "Accept": "application/json",
        "Content-Type": "application/json"
      },
      "httpContentType": "application/json"
    }
  ]
}   </MadCap:codeSnippetBody>
                            </MadCap:codeSnippet>
                        </td>
                    </tr>
                    <tr>
                        <td class="confluenceTd">
                            <p><strong>ValidateOtpJSON&#160;</strong>
                            </p>
                        </td>
                        <td class="confluenceTd">
                            <p>Specify this parameter to validate the OTP entered by an end user and determine if the verification is successful.</p>
                            <p>The binding variables to use are:</p>
                            <ul>
                                <li>
                                    <p><strong>${otpSelName}</strong>: Determines if the end-user has chosen email or SMS based OTP.</p>
                                </li>
                                <li>
                                    <p><strong>${passCode</strong>}: OTP entered by end-user during the verification process.</p>
                                </li>
                                <li>
                                    <p><strong>${user.username}</strong>,&#160;<strong>${user.firstname}</strong>, and so on: Fetches the end user’s profile properties.</p>
                                </li>
                            </ul>
                        </td>
                        <td class="confluenceTd">&#160;</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
</html>