﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" MadCap:onlyLocalStylesheets="False" class="concept">
    <head><title>User Import Preprocessor</title>
        <link href="../Resources/Stylesheets/Styles.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <h1><a name="top"></a>User Import Preprocessor</h1>
        <p>The user import process includes an inline preprocessor for normalizing the identity data using custom code. It transforms the data to bring it to such a state that IGA can easily parse. This helps you to circumvent the common issues that may be encountered while importing users including handling of special characters, usage of data from other tables of a database, and database locks. Cleaning the identity data ensures that clean and complete data is entered into Identity Repository. </p>
        <div class="confluence-information-macro confluence-information-macro-information conf-macro output-block">
            <div class="confluence-information-macro-body">
                <p>This feature is available from Release v5.5 onwards. If you are on an earlier version and want to use it, contact the Saviynt support team for guidance.&#160;</p>
            </div>
        </div>
        <p>The identity data can be broadly classified under <strong>Key Attributes</strong> and <strong>Additional Attributes</strong>.&#160;</p>
        <ul>
            <li>
                <p><strong>Key Attributes</strong> are the basic identity attributes of a user including First name, Last name, Location, and Email.</p>
            </li>
            <li>
                <p><strong>Additional Attributes</strong> are attributes derived from identity attributes or based on account attributes that are associated with an identity. From Release v5.5.0, IGA provides a framework for computing additional attributes. The framework provides the following advantages:</p>
                <ul style="list-style-type: circle;">
                    <li>
                        <p>It is clean and scalable.</p>
                    </li>
                    <li>
                        <p>It is generic and can be extended to all connectors that support identity import.</p>
                    </li>
                    <li>
                        <p>It is consolidated and configurable through <strong>MODIFYUSERDATAJSON</strong>.</p>
                    </li>
                </ul>
            </li>
        </ul>
        <p>In the previous versions of IGA, although identity attribute computation was supported but the solution components were not consolidated.&#160;</p>
        <p>You can use inline preprocessing for normalizing data in the following cases:</p>
        <ol>
            <li>
                <p>When you need to specify the organization unit based on CostCenter and Department of the identity (User) for provisioning an Active Directory account.</p>
            </li>
            <li>
                <p>When a User’s DisplayName is stored as PreferredName. If PreferredName is blank, the display name is stored in the <strong>LastName, FirstName</strong> format.</p>
            </li>
            <li>
                <p>When the status (Active/Inactive) of identity is based on HRStatus such as ShortTerm Leave, LongTerm Leave, PreHire, Terminated, or Active, which is provided in the HR feed data for that identity.</p>
            </li>
        </ol>
        <h2 id="UserImportPreprocessor-ConfiguringthePreprocessor">Configuring the Preprocessor</h2>
        <p>To configure the preprocessor, perform the following steps:</p>
        <ol>
            <li>
                <p>Configure&#160;the <strong>MODIFYUSERDATAJSON&#160;</strong>connection parameterfor the connector from the <strong>Add/Update Connection</strong>&#160;page (<strong>ADMIN &gt; Identity Repository &gt; Connections</strong>) using the following syntax:<br /></p>
                <div class="code panel pdl conf-macro output-block" style="border-width: 1px;">
                    <div class="codeContent panelContent pdl">
                        <MadCap:codeSnippet>
                            <MadCap:codeSnippetCopyButton aria-label="Copy" title="Copy to clipboard" />
                            <MadCap:codeSnippetCaption>Syntax</MadCap:codeSnippetCaption>
                            <MadCap:codeSnippetBody MadCap:useLineNumbers="True" MadCap:lineNumberStart="1" MadCap:continue="False" xml:space="preserve" style="padding-left: 12px;padding-right: 16px;padding-top: 12px;padding-bottom: 12px;mc-code-lang: PlainText;">{
  "ADDITIONALTABLES": {
    "&lt;SSM TableName&gt;": "&lt;SQL Query&gt;"  },
  "COMPUTEDCOLUMNS": [
    "&lt;Comma separated user columns to be computed&gt;"  ],
  "PREPROCESSQUERIES": [
    "UPDATE NEWUSERDATA SET &lt;SSM USER COLUMN NAME&gt;= &lt;SQL QUERY/FUNCTION&gt;",
    "UPDATE NEWUSERDATA SET &lt;SSM USER COLUMN NAME&gt;= (SSM TABLES ARE REFERRED AS CURRENT&lt;TABLENAME&gt;)",
    "CUSTOMFUNCTION###&lt;CUSTOM FUNCTION NAME&gt;"  ],
  "CUSTOMFUNCTIONS": {
    "&lt;FUNCTION NAME&gt;": {
      "FULLCLASSNAME": "&lt;CLASS NAME&gt;",
      "METHODNAME": "&lt;METHOD NAME&gt;"    }
  }
}
</MadCap:codeSnippetBody>
                        </MadCap:codeSnippet>
                    </div>
                </div>
                <p>
                    <br />Where,</p>
                <ul>
                    <li>
                        <p><code style="font-weight: bold;">ADDITIONALTABLES</code> is used to define the dataset required for executing queries. For example, as part of preprocessor queries following data is required:</p>
                        <ul style="list-style-type: circle;">
                            <li>
                                <p>User’s userkey, firstname, lastname <span class="attr">-&lt;&lt; “USERS”:”select userkey,firstname,lastname from users “&gt;&gt;</span><br style="font-weight: bold;" /><b>All the accounts and their associations with the user for the endpoint with endpointkey = 10&#160;-&lt;&lt; “ACCOUNTS”:”select * from accounts where endpointkey = 10 “&gt;&gt;</b></p>
                            </li>
                            <li>
                                <p><span class="attr">USER_ATTRIBUTES’s userkey, data, attributename - &lt;&lt;"USER_ATTRIBUTES": "SELECT USERKEY, DATA, ATTRIBUTENAME FROM USER_ATTRIBUTES"&gt;&gt;</span>
                                </p>
                                <div class="note" MadCap:autonum="&lt;b&gt;Note&lt;/b&gt;">
                                    <p class="note">
                                        <br /> From Release v2021.0 onwards, you can additionally use the preprocessor to update the <code style="font-weight: bold;">USER_ATTRIBUTES</code> table.</p>
                                </div>
                            </li>
                            <li>
                                <p><code style="font-weight: bold;">COMPUTEDCOLUMNS</code> lists identity attributes that need to be computed as part of the Identity import process. For example, customproperty2, customproperty3, customproperty4.</p>
                                <div class="note" MadCap:autonum="&lt;b&gt;Note&lt;/b&gt;">
                                    <p>To update <strong>secondarymanager</strong>, pass the username as per your requirement while using the preprocessor.<br />And for performance issues, create indexes on the conditional matching attributes.</p>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <p><code style="font-weight: bold;">PREPROCESSQUERIES</code> contains SQL query (with desired logic)&#160;for each of the computed columns identified in the <code style="font-weight: bold;">COMPUTEDCOLUMNS</code> section. To define this parameter, use a format similar to the following:<br /></p>
                        <div class="note" MadCap:autonum="&lt;b&gt;Note&lt;/b&gt;">
                            <ul style="list-style-type: circle;">
                                <li>
                                    <p>For Saviynt out-of-the-box table names, you must start the <strong>ENTITLEMENT_VALUES</strong> name with the&#160;<strong>CURRENT_</strong>&#160;prefix for the <code style="font-weight: bold;">PREPROCESSQUERIES</code> section. For example, <code style="font-weight: bold;">CURRENTENTITLEMENT_VALUES</code>.</p>
                                </li>
                                <li>
                                    <p>To execute the queries more quickly, you can create an index for a table in the <code style="font-weight: bold;">PREPROCESSQUERIES</code> section. For example, <span class="attr">"ALTER TABLE CURRENTUSERS ADD INDEX username (username ASC)"</span>.</p>
                                </li>
                                <li>
                                    <p>To filter unwanted data from a CSV file, you can add the following syntax in the <code style="font-weight: bold;">PREPROCESSQUERIES</code> section: <span class="attr">"delete from NEWUSERDATA where location is null or location =''"</span><br /></p>
                                </li>
                            </ul>
                            <div class="code panel pdl conf-macro output-block" style="border-width: 1px;">
                                <div class="codeContent panelContent pdl">
                                    <MadCap:codeSnippet>
                                        <MadCap:codeSnippetCopyButton aria-label="Copy" title="Copy to clipboard" />
                                        <MadCap:codeSnippetCaption>ConnectionUtilityService</MadCap:codeSnippetCaption>
                                        <MadCap:codeSnippetBody MadCap:useLineNumbers="True" MadCap:lineNumberStart="1" MadCap:continue="False" xml:space="preserve" style="padding-left: 12px;padding-right: 16px;padding-top: 12px;padding-bottom: 12px;mc-code-lang: Java;">{
  "ADDITIONALTABLES": {
    "USERS": "SELECT username,location,EMPLOYEECLASS,CUSTOMPROPERTY6,MIDDLENAME,SYSTEMUSERNAME from USERS ",
    "ENTITLEMENT_VALUES": "SELECT entitlementtypekey,customproperty1,customproperty2,customproperty3,customproperty4,customproperty5,customproperty6,customproperty7,entitlement_value FROM ENTITLEMENT_VALUES where entitlementtypekey in  (224,240)"  },
  "COMPUTEDCOLUMNS": [
    "CUSTOMPROPERTY1",
    "CITY",
    "COUNTRY",
    "CUSTOMPROPERTY2",
    "CUSTOMPROPERTY3",
    "STATE",
    "STREET",
    "CUSTOMPROPERTY4",
    "CUSTOMPROPERTY6",
    "SYSTEMUSERNAME"  ],
  "PREPROCESSQUERIES": [
    "ALTER TABLE CURRENTUSERS ADD INDEX username (username ASC)",
    "ALTER TABLE CURRENTUSERS ADD INDEX location (location ASC)",
    "ALTER TABLE CURRENTUSERS ADD INDEX EMPLOYEECLASS (EMPLOYEECLASS ASC)",
    "ALTER TABLE CURRENTENTITLEMENT_VALUES ADD INDEX entitlementtypekey (entitlementtypekey ASC)",
    "ALTER TABLE CURRENTENTITLEMENT_VALUES ADD INDEX entitlement_value (entitlement_value ASC)",
    "UPDATE NEWUSERDATA LEFT JOIN CURRENTUSERS ON NEWUSERDATA.USERNAME = CURRENTUSERS.USERNAME SET NEWUSERDATA.CUSTOMPROPERTY1=(select CURRENTENTITLEMENT_VALUES.customproperty1 from CURRENTENTITLEMENT_VALUES where CURRENTENTITLEMENT_VALUES.entitlementtypekey=240 and CURRENTENTITLEMENT_VALUES.entitlement_value =NEWUSERDATA.LOCATION) ",
    "UPDATE NEWUSERDATA LEFT JOIN CURRENTUSERS ON NEWUSERDATA.USERNAME = CURRENTUSERS.USERNAME SET NEWUSERDATA.CITY=(select CURRENTENTITLEMENT_VALUES.customproperty2 from CURRENTENTITLEMENT_VALUES where CURRENTENTITLEMENT_VALUES.entitlementtypekey=240 and CURRENTENTITLEMENT_VALUES.entitlement_value =NEWUSERDATA.LOCATION) ",
    "UPDATE NEWUSERDATA LEFT JOIN CURRENTUSERS ON NEWUSERDATA.USERNAME = CURRENTUSERS.USERNAME SET NEWUSERDATA.COUNTRY=(select CURRENTENTITLEMENT_VALUES.customproperty3 from CURRENTENTITLEMENT_VALUES where CURRENTENTITLEMENT_VALUES.entitlementtypekey=240 and CURRENTENTITLEMENT_VALUES.entitlement_value =NEWUSERDATA.LOCATION) ",
    "UPDATE NEWUSERDATA LEFT JOIN CURRENTUSERS ON NEWUSERDATA.USERNAME = CURRENTUSERS.USERNAME SET NEWUSERDATA.CUSTOMPROPERTY2=(select CURRENTENTITLEMENT_VALUES.customproperty4 from CURRENTENTITLEMENT_VALUES where CURRENTENTITLEMENT_VALUES.entitlementtypekey=240 and CURRENTENTITLEMENT_VALUES.entitlement_value =NEWUSERDATA.LOCATION) ",
    "UPDATE NEWUSERDATA LEFT JOIN CURRENTUSERS ON NEWUSERDATA.USERNAME = CURRENTUSERS.USERNAME SET NEWUSERDATA.CUSTOMPROPERTY3=(select CURRENTENTITLEMENT_VALUES.customproperty5 from CURRENTENTITLEMENT_VALUES where CURRENTENTITLEMENT_VALUES.entitlementtypekey=240 and CURRENTENTITLEMENT_VALUES.entitlement_value =NEWUSERDATA.LOCATION) ",
    "UPDATE NEWUSERDATA LEFT JOIN CURRENTUSERS ON NEWUSERDATA.USERNAME = CURRENTUSERS.USERNAME SET NEWUSERDATA.STATE=(select CURRENTENTITLEMENT_VALUES.customproperty6 from CURRENTENTITLEMENT_VALUES where CURRENTENTITLEMENT_VALUES.entitlementtypekey=240 and CURRENTENTITLEMENT_VALUES.entitlement_value =NEWUSERDATA.LOCATION) ",
    "UPDATE NEWUSERDATA LEFT JOIN CURRENTUSERS ON NEWUSERDATA.USERNAME = CURRENTUSERS.USERNAME SET NEWUSERDATA.STREET=(select CURRENTENTITLEMENT_VALUES.customproperty7 from CURRENTENTITLEMENT_VALUES where CURRENTENTITLEMENT_VALUES.entitlementtypekey=240 and CURRENTENTITLEMENT_VALUES.entitlement_value =NEWUSERDATA.LOCATION) ",
    "UPDATE NEWUSERDATA LEFT JOIN CURRENTUSERS ON NEWUSERDATA.USERNAME = CURRENTUSERS.USERNAME SET NEWUSERDATA.CUSTOMPROPERTY4=(select CURRENTENTITLEMENT_VALUES.entitlement_value from CURRENTENTITLEMENT_VALUES where CURRENTENTITLEMENT_VALUES.entitlementtypekey=224 and CURRENTENTITLEMENT_VALUES.customproperty1 LIKE CONCAT('%', NEWUSERDATA.LOCATION, '%')and CURRENTENTITLEMENT_VALUES.customproperty2 LIKE CONCAT('%', NEWUSERDATA.EMPLOYEECLASS, '%')) ",
    "UPDATE NEWUSERDATA LEFT JOIN CURRENTUSERS ON NEWUSERDATA.USERNAME = CURRENTUSERS.USERNAME SET NEWUSERDATA.CUSTOMPROPERTY6 = case when  CURRENTUSERS.CUSTOMPROPERTY6 is not null and CURRENTUSERS.CUSTOMPROPERTY6 != '' then CURRENTUSERS.CUSTOMPROPERTY6 when (CURRENTUSERS.CUSTOMPROPERTY6 IS NULL or CURRENTUSERS.CUSTOMPROPERTY6 = '') AND (NEWUSERDATA.MIDDLENAME IS NOT NULL and NEWUSERDATA.MIDDLENAME != '') THEN CONCAT(CONCAT(UPPER(SUBSTRING(NEWUSERDATA.firstname,1,1)),LOWER(SUBSTRING(NEWUSERDATA.firstname,2))),' ',UPPER(SUBSTRING(NEWUSERDATA.middlename,1,1)),'.',' ',CONCAT(UPPER(SUBSTRING(NEWUSERDATA.lastname,1,1)),LOWER(SUBSTRING(NEWUSERDATA.lastname,2)))) WHEN (CURRENTUSERS.CUSTOMPROPERTY6 IS NULL or CURRENTUSERS.CUSTOMPROPERTY6='') AND (NEWUSERDATA.MIDDLENAME IS  NULL or NEWUSERDATA.MIDDLENAME ='') THEN CONCAT(CONCAT(UPPER(SUBSTRING(NEWUSERDATA.firstname,1,1)),LOWER(SUBSTRING(NEWUSERDATA.firstname,2))),' ',CONCAT(UPPER(SUBSTRING(NEWUSERDATA.lastname,1,1)),LOWER(SUBSTRING(NEWUSERDATA.lastname,2)))) end",
    "CUSTOMFUNCTION###FUNCTION1"  ],
  "CUSTOMFUNCTIONS": {
    "FUNCTION1": {
      "FULLCLASSNAME": "com.saviynt.utility.Transformation",
      "METHODNAME": "doCustomPreprocess"    }
  }
}</MadCap:codeSnippetBody>
                                    </MadCap:codeSnippet>
                                </div>
                            </div>
                            <p class="note">
                                <br />You have to package the custom java code in the jar file and deploy this file at the following location:<br /><label class="filename-path">$(tomcathome)/webapps/ECM/WEB-INF/lib</label> directory on Saviynt server Raise a Freshdesk ticket and share these details with the Saviynt Support team.</p>
                        </div>
                    </li>
                </ul>
            </li>
            <li>
                <p>Update the <strong>MODIFYUSERJSON</strong> connection parameter with the user details provided in the .sav schema file. To update this parameter, use a format similar to the following:</p>
                <div class="codeContent panelContent pdl">
                    <MadCap:codeSnippet>
                        <MadCap:codeSnippetCopyButton aria-label="Copy" title="Copy to clipboard" />
                        <MadCap:codeSnippetBody MadCap:useLineNumbers="True" MadCap:lineNumberStart="1" MadCap:continue="False" xml:space="preserve" style="padding-left: 12px;padding-right: 16px;padding-top: 12px;padding-bottom: 12px;mc-code-lang: PlainText;">"USERNAME",
"SYSTEMUSERNAME",
"FIRSTNAME",
"LASTNAME",
"STATUSKEY#USER_NOT_IN_FILE_ACTION=NOACTION#DELIMITER=",
#SKIP_NUMBER_OF_LINES=1#ZERODAYPROVISIONING=FALSE#CHECKRULES=FALSE#GENERATESYSTEMUSERNAME=FALSE#BUILDUSERMAP=FALSE#MODIFYUSERDATAJSON={
   "ADDITIONALTABLES":{
      "USERS":"SELECT USERKEY FROM USERS"   },
   "COMPUTEDCOLUMNS":[
      "customproperty1",
      "customproperty2",
      "customproperty3"   ],
   "PREPROCESSQUERIES":[
      "UPDATE NEWUSERDATA SET CUSTOMPROPERTY3 = (select count(USERKEY) from currentusers)",
      "CUSTOMFUNCTION###FUNCTION1"   ],
   "CUSTOMFUNCTIONS":{
      "FUNCTION1":{
         "FULLCLASSNAME":"com.saviynt.utility.connection.ConnectionUtlityService",
         "METHODNAME":"doCustomPreprocess"      }
   }
}</MadCap:codeSnippetBody>
                    </MadCap:codeSnippet>
                </div>
            </li>
        </ol>
        <p> 3. Specify the external table connection details in the <label class="filename-path">config.groovy</label> file if you are using an external jar and require a table for transformation.</p>
        <p>To specify the details, use a format similar to the following:</p>
        <MadCap:codeSnippet>
            <MadCap:codeSnippetCopyButton aria-label="Copy" title="Copy to clipboard" />
            <MadCap:codeSnippetCaption>SQL</MadCap:codeSnippetCaption>
            <MadCap:codeSnippetBody MadCap:useLineNumbers="False" MadCap:lineNumberStart="1" MadCap:continue="False" xml:space="preserve" style="padding-left: 12px;padding-right: 16px;padding-top: 12px;padding-bottom: 12px;mc-code-lang: PlainText;">extSQLConnection {username = "ssmdau"password = ""passwordEncrypted = "false"url = "jdbc:mysql://localhost:3306/ssminlp?serverTimezone=UTC&amp;useUnicode=yes&amp;characterEncoding=UTF-8"driverClassName = "com.mysql.jdbc.Driver"}</MadCap:codeSnippetBody>
        </MadCap:codeSnippet>
        <ul>
            <li>
                <p><code style="font-weight: bold;">username</code> and <code style="font-weight: bold;">password</code> are the same as specified for data analyzer. The user must have read-write permission for the schema created for data processing.</p>
            </li>
            <li>
                <p><code style="font-weight: bold;">passwordEncrypted = "false"</code> is used to pass unencrypted password.</p>
            </li>
            <li>
                <p><code style="font-weight: bold;">passwordEncrypted = "true"</code> is used to pass encrypted password.</p>
            </li>
        </ul>
        <div class="note" MadCap:autonum="&lt;b&gt;Note&lt;/b&gt;">
            <p class="note">Raise a Freshdesk ticket with the Saviynt Support team and get the externalSQLConnection details if they are not found in <label class="filename-path">Config.groovy</label> file.</p>
        </div>
        <p>After the configuration steps are completed, your preprocessor is ready for normalizing the identity data.</p>
        <h2>Using the Preprocessor</h2>
        <p>You can use the preprocessor in the following ways:</p>
        <ul>
            <li>
                <p>To import user using a connector, go to <strong>ADMIN &gt; Identity Repository &gt; Connections</strong>, select <strong>Action &gt; Create Connection </strong>and specify the value in the connection parameter <strong>MODIFYUSERDATAJSON</strong>.<br /></p>
            </li>
            <li>
                <p>To upload users from IGA, go to <strong>Admin &gt; Identity Repository &gt; Users &gt; Action &gt; Upload User</strong> and specify the value in the parameter <strong>User Pre-processor Config JSON</strong>.<br />For more information, see <MadCap:xref href="Uploading-Users-using-File-Based-Method.htm">Uploading Users using File Based Method</MadCap:xref>.</p>
                <p>
                    <img src="../Resources/Images/48.png" style="border-left-style: solid;border-left-width: 1px;border-left-color: ;border-right-style: solid;border-right-width: 1px;border-right-color: ;border-top-style: solid;border-top-width: 1px;border-top-color: ;border-bottom-style: solid;border-bottom-width: 1px;border-bottom-color: ;" />
                    <br />
                </p>
            </li>
            <li>
                <p>To import using the SAV file, place the SAV schema file for mapping the user details added in the CSV file, the database user table fields and the <strong>MODIFYUSERDATAJSON</strong> in a directory and run the <strong>File based Users Import (SchemaUserJob)</strong>.<br />For more information, see <MadCap:xref href="Uploading-Users-using-Schema-Upload.htm">Uploading Users using Schema Upload</MadCap:xref>.</p>
                <p>
                    <img src="../Resources/Images/49.png" style="border-left-style: solid;border-left-width: 1px;border-left-color: ;border-right-style: solid;border-right-width: 1px;border-right-color: ;border-top-style: solid;border-top-width: 1px;border-top-color: ;border-bottom-style: solid;border-bottom-width: 1px;border-bottom-color: ;" />
                </p>
            </li>
        </ul>
        <h2>FAQ</h2>
        <p>This section serves as a supplement to the documentation and answers the most commonly asked questions on user import using the preprocessor.</p>
        <ol>
            <li>
                <p><strong>How to use custom functions defined in the database during user preprocessing?</strong>
                    <br />You can define the custom function in the database and call it directly with its name as an SQL function.</p>
            </li>
            <li>
                <p><strong>At what stage is the Java method called?</strong>
                    <br />The Java method is called while performing the preprocessing queries.</p>
            </li>
            <li>
                <p><strong>Is the Java method called for each user record? Can we pass any arguments to the Java method and return any values? </strong>
                    <br />Java method is called only once as defined in the JSON, not for each individual user. Currently, we do not support passing any custom arguments to the main Java method. It takes in a set of pre-defined parameters i.e., Connection connection, Map tempTableNamesMap.</p>
            </li>
        </ol>
    </body>
</html>