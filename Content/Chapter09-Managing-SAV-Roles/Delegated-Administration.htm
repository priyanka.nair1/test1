﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head><title>Delegated Administration</title>
        <link href="../Resources/TableStyles/Default-Table.css" rel="stylesheet" MadCap:stylesheetType="table" />
        <link href="../Resources/Stylesheets/Styles.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <h1>Delegated Administration</h1>
        <ul>
            <li>
                <p>
                    <MadCap:xref href="#Overview">Overview</MadCap:xref>
                </p>
            </li>
            <li>
                <p>
                    <MadCap:xref href="#Delegate">Delegated Administration Use Cases</MadCap:xref>
                </p>
            </li>
            <li>
                <p>
                    <MadCap:xref href="#Persona-">Persona-Based View of Analytics Reports&#160;</MadCap:xref>
                </p>
            </li>
            <li>
                <p>
                    <MadCap:xref href="#Analytic">Analytics Reports based on&#160;Organization Context</MadCap:xref>
                </p>
            </li>
        </ul>
        <h2><a name="Overview"></a>Overview</h2>
        <p>Saviynt Identity Governance and Administration (IGA)&#160;provides you a reliable&#160;security solution of&#160;delegated administration that&#160;provides role-based access to users to access specific information and administrative functions within IGA.</p>
        <p>This delegated administration solution enables specific users or a group of users to view and manage identity repository objects within IGA&#160;based on the SAV role configuration of the users. Examples of identity repository objects can be entitlements, endpoints, organizations, security systems, or accounts.</p>
        <p>For example, you can configure application owners in IGA who are users having access to specific applications or security systems. Suppose each application owner in an application support team wants to manage entitlements belonging to their respective applications.&#160;When delegated administration is configured, it enables each application owner to become the delegated administrator and manage his own entitlements and accounts from IGA. This reduces the overhead on one person (say, the manager of application owners) to manage the admin functions of individual application owners. Based on the default SAV role configuration of the application owner, the delegated administration configuration&#160;segregates information (data) and administrative functions at the application level and the role level.</p>
        <h3>Advantages of Delegated Administration</h3>
        <p>The delegated administration solution provides the following benefits to users:</p>
        <ul>
            <li>Reducing the overhead and&#160;Total Cost of Ownership (TCO) by delegating the administration tasks within IGA.</li>
            <li>
                <p>Faster turnaround time for the change requests and change management on identity repository objects.</p>
            </li>
            <li>Restricted visibility of&#160;various metadata within IGA based on role.</li>
        </ul>
        <h3>Commonly Used Terms</h3>
        <p>The following terms are commonly used in this document:</p>
        <ul>
            <li>
                <p>Entitlement refers to the access that a user can request in an endpoint. To be specific, an entitlement represents the level of privileges (read, write, execute) which is assigned for an application.</p>
            </li>
            <li>
                <p>An endpoint is a representation of the third-party application in IGA from which accounts and entitlements (access) are imported to IGA.</p>
            </li>
            <li>
                <p>To better manage the allocation of entitlements, entitlements are assigned to a role. You can assign single or multiple entitlements to a role.</p>
            </li>
            <li>
                <p>The security system represents the connection between IGA and the target application. For example, AD, LDAP, AWS, Azure, Box are examples of security systems. A security system comprises an endpoint, which is the actual target application for which IGA manages the identity repository.</p>
            </li>
            <li>
                <p>Connection refers to the configuration setup of IGA connecting to target applications. Examples include AD, Database, LDAP, SAP, PeopleSoft connection, etc.</p>
            </li>
            <li>
                <p>SAV roles define the privileges available to the role assignee, specifying their accessibility and the functional limit on an application.</p>
            </li>
        </ul>
        <p>The delegated administration capabilities in IGA can be broadly classified into two logical categories:</p>
        <ul>
            <li>
                <p>Delegating and managing admin data (for example, entitlement management). For more information, see <MadCap:xref href="#Delegate">Delegated Administration Use Cases</MadCap:xref>.</p>
            </li>
            <li>
                <p>Managing IGA functionalities specific to modules such as Access Request System and Analytics. For more information, see <MadCap:xref href="#Persona-">Persona-Based View of Analytics Reports&#160;</MadCap:xref>.</p>
            </li>
        </ul>
        <h2><a name="Delegate"></a>Delegated Administration Use Cases</h2>
        <p>This section provides one of the use cases for delegated administration of entitlements and the possible solution configuration in IGA.</p>
        <h3 id="DelegatedAdministration-UseCase:DelegatedAdministrationforApplicationManagement">Use Case:&#160;Delegated Administration for Application Management</h3>
        <p>As an admin user, suppose you want to grant access to application owners to manage their own application data.  &#160;Each user should be able to manage their applications only and any user should not modify&#160;entitlement metadata of any other user.&#160;For example, an application owner can manage the accounts, entitlements, endpoints, etc. belonging to an application that he owns.</p>
        <p><strong>Solution:&#160;</strong>Using delegated administration framework in IGA, you can restrict specific users to access only the required applications. This is configured by associating the SAV role of the users, connection, and security system.</p>
        <p><strong>Solution Configuration in IGA:</strong>
        </p>
        <p>To configure delegated administration for entitlement management in IGA, you need to associate the security system with a connection. The connection, in turn, should be associated with the SAV role of the user. This SAV role is added as the default SAV role of the connection.</p>
        <p>The configuration association of security system, connection, and SAV role ensures that from&#160;the Admin section of IGA, users belonging to this SAV role can view or manage only those identity repository objects (say, entitlements) that belong to the associated security system.</p>
        <p>
            <img src="../Resources/Images/Associated Security System.png" style="border-left-style: solid;border-left-width: 1px;border-right-style: solid;border-right-width: 1px;border-top-style: solid;border-top-width: 1px;border-bottom-style: solid;border-bottom-width: 1px;" />
        </p>
        <p>For example, if the default SAV role of an application owner is associated with a connection and security system, then this application owner can manage only those entitlements and accounts that belong to the associated security system. The access privileges of the application owners are then determined by the Request Map associated with the SAV role.</p>
        <p>
            <img src="../Resources/Images/Request_Map_Associated_SAV_Role.png" style="border-left-style: solid;border-left-width: 1px;border-right-style: solid;border-right-width: 1px;border-top-style: solid;border-top-width: 1px;border-bottom-style: solid;border-bottom-width: 1px;" />
        </p>
        <p>The delegated administration solution thus helps the application owners to exclusively view and modify the identity repository objects (for example,&#160;entitlements)&#160;that are associated with their SAV role.&#160;</p>
        <p>However, there are some limitations to this model. There are no specific restrictions on the entitlements that an application owner can create, for example, if the SAV role associated with the application owner provides access privileges to create entitlements using a spreadsheet, then the application owner can create entitlements for other applications as well.&#160;Hence this model is recommended to be used only when the application owners want to manage their own&#160;entitlements for their applications and not for creating new entitlements.</p>
        <h3 id="DelegatedAdministration-ExampleConfigurationforDelegatingApplicationManagement">Example Configuration for&#160;Delegating Application Management</h3>
        <p>This section provides an example related to the configuration of delegated administration of entitlements in IGA.&#160;This use case is about restricting application owners to manage only their own applications and related entitlements.&#160;An application owner is the resource owner who can be an individual user or user group who is primarily responsible for request approvals in workflows for the respective endpoint.&#160;With this configuration, the application owners are not allowed to modify the entitlement metadata of other application owners.</p>
        <h4 id="DelegatedAdministration-AddingApplicationOwnerstotheSAVRole">Adding Application Owners to the SAV Role</h4>
        <p>You can associate an application owner with a SAV role from the Users tab in a SAV role. This can be configured from&#160;<strong>Admin &gt;&#160;SAV Roles &gt; Users </strong>tab.</p>
        <p>This SAV role is used as the default SAV role for the connection.</p>
        <div class="note" MadCap:autonum="&lt;b&gt;Note&lt;/b&gt;">
            <p class="note">For more information about configuring Users tab in a SAV role, see Users Tab in&#160;.</p>
        </div>
        <h4 id="DelegatedAdministration-AssociatingtheDefaultSAVRoleforaConnection">Associating the Default SAV Role for a Connection</h4>
        <p>In the required connection, you can associate the default SAV role in the field <strong>Default SAV Role</strong>&#160;as shown in the following figure.</p>
        <p>
            <img src="../Resources/Images/Default_SAV_Role_Connection.png" style="border-left-style: solid;border-left-width: 1px;border-right-style: solid;border-right-width: 1px;border-top-style: solid;border-top-width: 1px;border-bottom-style: solid;border-bottom-width: 1px;width: 800px;height: 500px;" />
        </p>
        <div class="note" MadCap:autonum="&lt;b&gt;Note&lt;/b&gt;">
            <p class="note">For more information about configuring connections, see&#160;<MadCap:xref href="../Chapter04-Onboarding-and-Managing-Applications/5.5.x/Creating-a-Connection.htm">Creating a Connection</MadCap:xref>.</p>
        </div>
        <h4 id="DelegatedAdministration-AssociatingaConnectionwiththeSecuritySystem">Associating a Connection with the Security System</h4>
        <p>From the <strong>Admin</strong> section of IGA, you can associate a connection with the required security system.&#160;</p>
        <p>
            <img src="../Resources/Images/Default_SAV_Role_System.png" style="border-left-style: solid;border-left-width: 1px;border-right-style: solid;border-right-width: 1px;border-top-style: solid;border-top-width: 1px;border-bottom-style: solid;border-bottom-width: 1px;width: 800px;height: 500px;" />
        </p>
        <div class="note" MadCap:autonum="&lt;b&gt;Note&lt;/b&gt;">
            <p class="note"> For more information about creating a security system, see <MadCap:xref href="../Chapter04-Onboarding-and-Managing-Applications/5.5.x/Creating-a-Security-System.htm">Creating a Security System</MadCap:xref>. &#160; &#160;</p>
        </div>
        <h4 id="DelegatedAdministration-DefiningSAVRolePermissions">Defining SAV Role Permissions</h4>
        <p>The objects that need to be accessed by the Application Owners determine the access privileges for that specific SAV role. For example, if Application Owners are granted access to manage the entitlements in their applications, the permissions (requestmap) can be associated with the SAV role, as shown in the following figure. The application owners can manage only the entitlements belonging to their own applications.</p>
        <p>
            <img src="../Resources/Images/SAV_Role_Permission.png" style="border-left-style: solid;border-left-width: 1px;border-right-style: solid;border-right-width: 1px;border-top-style: solid;border-top-width: 1px;border-bottom-style: solid;border-bottom-width: 1px;width: 800px;height: 500px;" />
        </p>
        <div class="note" MadCap:autonum="&lt;b&gt;Note&lt;/b&gt;">
            <p class="note">For more information about configuring <strong>Access</strong>&#160;tab in a SAV role, see <MadCap:xref href="Understanding-the-SAV-Role-Parameters.htm#Access">Access Tab</MadCap:xref>.</p>
        </div>
        <h3 id="DelegatedAdministration-PossibleDelegatedAdminFunctionsinEIC">Possible Delegated Admin Functions in IGA</h3>
        <p>The users who carry out delegation functions are referred to as delegated administrators or delegated admin, in this document.</p>
        <p>The following table explains the possible admin functions in IGA that are available to the users who are configured as delegated administrators.&#160;</p>
        <div class="table-wrap">
            <table class="TableStyle-Default-Table" style="mc-table-style: url('../Resources/TableStyles/Default-Table.css');width: 100%;" cellspacing="0">
                <col style="width: 19.0553%;" class="TableStyle-Default-Table-Column-Column1" />
                <col style="width: 80.7837%;" class="TableStyle-Default-Table-Column-Column1" />
                <thead>
                    <tr class="TableStyle-Default-Table-Head-Header1">
                        <th class="TableStyle-Default-Table-HeadE-Column1-Header1">Identity Repository Object/ Function</th>
                        <th class="TableStyle-Default-Table-HeadD-Column1-Header1">Possible Admin Functions available to the Delegated Administrator</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="TableStyle-Default-Table-Body-Body1">
                        <td class="TableStyle-Default-Table-BodyE-Column1-Body1">Entitlements&#160;</td>
                        <td class="TableStyle-Default-Table-BodyD-Column1-Body1">
                            <p class="table-text">The delegated admin can view all entitlements in an application.</p>
                            <p class="table-text">The delegated admin can also update entitlement metadata like Display name, Description, Risk, Custom Properties and add/ modify Entitlement owner.</p>
                        </td>
                    </tr>
                    <tr class="TableStyle-Default-Table-Body-Body1">
                        <td class="TableStyle-Default-Table-BodyE-Column1-Body1">Organizations</td>
                        <td class="TableStyle-Default-Table-BodyD-Column1-Body1">
                            <p class="table-text">The delegated admin can view all Organizations related to an application and also update metadata like name and description of the organization.</p>
                        </td>
                    </tr>
                    <tr class="TableStyle-Default-Table-Body-Body1">
                        <td class="TableStyle-Default-Table-BodyE-Column1-Body1">Accounts</td>
                        <td class="TableStyle-Default-Table-BodyD-Column1-Body1">The delegated admin can view all accounts of an application and also map users/owners to the accounts that they own.</td>
                    </tr>
                    <tr class="TableStyle-Default-Table-Body-Body1">
                        <td colspan="1" class="TableStyle-Default-Table-BodyE-Column1-Body1">Security Systems</td>
                        <td colspan="1" class="TableStyle-Default-Table-BodyD-Column1-Body1">The delegated admin can view a limited set of security systems and associated details. The delegated admin can also update the security system details.&#160;</td>
                    </tr>
                    <tr class="TableStyle-Default-Table-Body-Body1">
                        <td colspan="1" class="TableStyle-Default-Table-BodyB-Column1-Body1">Endpoints</td>
                        <td colspan="1" class="TableStyle-Default-Table-BodyA-Column1-Body1">
                            <p class="table-text">The delegated admin can view a limited set of&#160;endpoints and associated details. The delegated admin can also update endpoint details including custom properties.&#160;</p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <h2 id="DelegatedAdministration-Persona-BasedViewofAnalyticsReports"><a name="Persona-"></a>Persona-Based View of Analytics Reports&#160;</h2>
        <p>Identity Governance and Administration (IGA) provides you the capability to view required data in Analytics Reports based on the context of the user who is logged in to IGA.&#160;</p>
        <p>The context can be <strong>User Context </strong>or <strong>Organizational Context</strong>.&#160;</p>
        <p>In an analytic report, User Context provides a personalized view of records for each user based on the userkey whereas the Organization Context allows you to group a set of records for a specific set of users.&#160;</p>
        <p>These concepts are explained in detail using examples in the following sections.</p>
        <h3>Analytics Reports based on User Context</h3>
        <p>When using the user context for viewing Elastic Search-based analytics reports, the visibility of data displayed in the report depends on the context of the user and is very specific to that user only.&#160;Depending on the userkey of the logged-in user, IGA displays the relevant data in the ES Analytics Report.&#160;</p>
        <p>As shown in the following figure, when user context is configured for the ES-based Analytics Report, the report view is different for each user depending on the user key associated with the user.</p>
        <p>
            <img src="../Resources/Images/User Context.png" style="border-left-style: solid;border-left-width: 1px;border-right-style: solid;border-right-width: 1px;border-top-style: solid;border-top-width: 1px;border-bottom-style: solid;border-bottom-width: 1px;" />
        </p>
        <h3>Advantages of User Context-based ES Analytics Reports</h3>
        <p>The configuration of ES Analytics Reports based on user context provides you the following benefits:</p>
        <ul>
            <li>Enhanced security posture by limiting the data visibility to specific users</li>
            <li>Enriched user experience by displaying only relevant data in the report view&#160;</li>
            <li>Reduction in the creation of redundant app-based reports.&#160;</li>
        </ul>
        <h3>Example Use Case for User Context-based Analytics Reports</h3>
        <p><strong>Use Case:&#160;</strong>Suppose there are multiple teams in an organization and the manager of each team wants to view Analytics report only with data for each member in his team. As an admin user, you want to make the required configurations in IGA so that each manager user can view the analytics report data for their direct reports only.</p>
        <p><strong>Solution:</strong>&#160;The report based on user context is beneficial in this use case because based on the user context, IGA can create a single report which can be viewed by all managers. Each manager can view the same report, but the number of reports and the data displayed in each report remains specific to each manager.</p>
        <h4>Sample Configuration: User-Context based ES Report</h4>
        <p>Suppose there is an admin user who has ROLE_ADMIN as the SAV role. This user can view ES-based reports configured for all the users in IGA.</p>
        <p>
            <img src="../Resources/Images/User-Context based ES Report.png" style="border-left-style: solid;border-left-width: 1px;border-right-style: solid;border-right-width: 1px;border-top-style: solid;border-top-width: 1px;border-bottom-style: solid;border-bottom-width: 1px;" />
        </p>
        <p>Figure: Selecting a sample ES-based Report report from Analytics History V2 (for an admin user)</p>
        <p>
            <img src="../Resources/Images/Admin view of ES-based report.png" style="border-left-style: solid;border-left-width: 1px;border-right-style: solid;border-right-width: 1px;border-top-style: solid;border-top-width: 1px;border-bottom-style: solid;border-bottom-width: 1px;width: 800px;height: 500px;" />
        </p>
        <p>Figure: Admin view of ES-based report (sample) showing all records&#160;&#160;</p>
        <p>Suppose a manager wants to view the same report. Note that the manager has a SAV role different from ROLE_ADMIN.</p>
        <p>This manager can view the same report from <strong>Analytics History V2</strong>  &#160;&#160;but the view is limited only to the records of those team members who directly report to him.</p>
        <p>
            <img src="../Resources/Images/Manager_s view of the same&#160;ES-based report.png" style="border-left-style: solid;border-left-width: 1px;border-right-style: solid;border-right-width: 1px;border-top-style: solid;border-top-width: 1px;border-bottom-style: solid;border-bottom-width: 1px;width: 800px;height: 500px;" />
        </p>
        <p>Figure: Manager's view of the same&#160;ES-based report&#160; but with limited records</p>
        <div class="note" MadCap:autonum="&lt;b&gt;Note&lt;/b&gt;">
            <p class="note">The number of user records (7 entries seen in the preceding figure) is the same as the list seen from the <strong>Users</strong> tab for the SAV role of the manager.&#160;To view the users configured for that SAV role, select&#160;<strong>Admin &gt;&#160;SAV Roles</strong>,&#160;and then select the SAV Role of the manager, and click<strong> Users </strong>tab.</p>
        </div>
        <h3>Configuring User Context-based ES Analytics Reports in IGA</h3>
        <p>To configure user context in Analytics reports, you need to perform the following configurations&#160;while creating the analytics control from <strong style="text-align: left;">Create New Analytics Configuration</strong>page:</p>
        <ul>
            <li>Provide an SQL query with the keyword USERCONTEXT with the userkey in the <strong>Analytics Query</strong> field and&#160;</li>
            <li>Select the <strong>Context</strong> field as <strong>User</strong>.</li>
        </ul>
        <p>The following figures display the configuration of the user context for a sample ES Analytics Report.</p>
        <p>Here the userkey "manager" is associated with the keyword USERCONTEXT in the SQL Query in&#160;<strong>Analytics Query</strong> field.</p>
        <p>
            <img src="../Resources/Images/Analytics Query.png" style="border-left-style: solid;border-left-width: 1px;border-right-style: solid;border-right-width: 1px;border-top-style: solid;border-top-width: 1px;border-bottom-style: solid;border-bottom-width: 1px;" />
        </p>
        <p>Figure: Including the keyword USERCONTEXT with userkey in the Analytics Query</p>
        <p>
            <img src="../Resources/Images/Selecting the Context field.png" style="border-left-style: solid;border-left-width: 1px;border-right-style: solid;border-right-width: 1px;border-top-style: solid;border-top-width: 1px;border-bottom-style: solid;border-bottom-width: 1px;" />
        </p>
        <p>Figure: Selecting the Context field as User</p>
        <div class="note" MadCap:autonum="&lt;b&gt;Note&lt;/b&gt;">
            <p class="note">Elastic Search-based analytics reports are also known as ES-based Analytics or Version 2 Analytics.</p>
        </div>
        <p>For more information about creating ES-based analytics controls, see&#160;<MadCap:xref href="../Chapter16-Managing-EIC-Analytics/Creating-Elasticsearch-based-Analytics-Control-V2-using-SQL-Query.htm">Creating Elasticsearch-based Analytics Controls (Version 2) using SQL Query</MadCap:xref>.</p>
        <h2 id="DelegatedAdministration-AnalyticsReportsbasedonOrganizationContext"><a name="Analytic"></a>Analytics Reports based on&#160;Organization Context</h2>
        <p class="auto-cursor-target">When the organization context is configured for an analytics report, the visibility of data is primarily driven by the Organizations object in Saviynt Identity Repository. In this configuration, an organization should contain the associated endpoints and it should also be part of a SAV role. The users who are assigned to this SAV role has access to all endpoints that are associated with that organization.&#160;This helps an organization to grant access to a specific set of users (or group of users) to specific endpoints.</p>
        <p class="auto-cursor-target">The following figure shows the association of SAV Role, Organization, and Endpoints required for delegated administration of Analytics Reports with an organizational context.</p>
        <p class="auto-cursor-target">
            <img src="../Resources/Images/Association of SAV Role.png" style="border-left-style: solid;border-left-width: 1px;border-right-style: solid;border-right-width: 1px;border-top-style: solid;border-top-width: 1px;border-bottom-style: solid;border-bottom-width: 1px;" />
        </p>
        <p class="auto-cursor-target">Figure: Association of SAV Role, Organization, and Endpoints&#160;</p>
        <p class="auto-cursor-target">You can associate multiple endpoints with an organization. Similarly, you can also associate one or more organizations with an SAV Role.&#160;</p>
        <p class="auto-cursor-target">Thus, you can group a set of records in an analytics control report based on endpoints, and assign that set of records as a "view" to a group of users through a common SAV role.&#160;There can be multiple analytic control records grouped together and multiple users can access the same records.&#160;</p>
        <h3>Configurations for Associating SAV Role, Organizations, Endpoints and Users</h3>
        <p class="auto-cursor-target">You can view and associate the SAV Role with a user from<strong> Admin &gt; Identity Repository &gt; Users &gt; SAV Role</strong> tab.</p>
        <p class="auto-cursor-target">
            <img src="../Resources/Images/Associating User with SAV Role.png" style="border-left-style: solid;border-left-width: 1px;border-right-style: solid;border-right-width: 1px;border-top-style: solid;border-top-width: 1px;border-bottom-style: solid;border-bottom-width: 1px;width: 800px;height: 500px;" />
        </p>
        <p class="auto-cursor-target">Figure: Associating User with SAV Role</p>
        <p class="auto-cursor-target">From the SAV Roles, you can associate the organization from <strong>Organizations</strong> field in SAV Role Detail tab.&#160;</p>
        <p class="auto-cursor-target">
            <img src="../Resources/Images/Associating Organization with SAV Role.png" style="border-left-style: solid;border-left-width: 1px;border-right-style: solid;border-right-width: 1px;border-top-style: solid;border-top-width: 1px;border-bottom-style: solid;border-bottom-width: 1px;width: 800px;height: 500px;" />
        </p>
        <p class="auto-cursor-target">Figure: Associating Organization with SAV Role</p>
        <h3>Example Configuration: Organization-Context based ES Report</h3>
        <p class="auto-cursor-target"><strong>Use Case:</strong> Suppose an admin user wants to restrict the view of analytics control records for the managers in each department of an organization.&#160;</p>
        <p class="auto-cursor-target">Suppose there is an admin user who has ROLE_ADMIN as the SAV role. As shown in the following figure, this user can view all the records in an ES Analytics Report from the <strong>Analytics History</strong> page.&#160;There are multiple endpoints and each endpoint shows many records (for example, 2295 for the example endpoint leasepurchasesystem).</p>
        <p class="auto-cursor-target">
            <img src="../Resources/Images/Sample Analytics History detail.png" style="border-left-style: solid;border-left-width: 1px;border-right-style: solid;border-right-width: 1px;border-top-style: solid;border-top-width: 1px;border-bottom-style: solid;border-bottom-width: 1px;width: 800px;height: 500px;" />
        </p>
        <p class="auto-cursor-target">Figure: Sample Analytics History detail of an admin user</p>
        <p class="auto-cursor-target">But for a manager user, there are only specific endpoints seen and the corresponding records are displayed.</p>
        <p class="auto-cursor-target">
            <img src="../Resources/Images/Sample Analytics History detail of a manager user.png" style="width: 800px;height: 400px;" />
        </p>
        <p class="auto-cursor-target">Figure:&#160;Sample Analytics History detail of a manager user</p>
        <p class="auto-cursor-target">The manager has a limited view of records because the organization configured in the SAV Role of the manager is only associated with one endpoint (for example, mts trade nom) as shown in the figure.</p>
        <p class="auto-cursor-target">
            <img src="../Resources/Images/SAV Role Detail of the manager.png" style="width: 800px;height: 600px;" />
        </p>
        <p class="auto-cursor-target">Figure: SAV Role Detail of the manager</p>
        <p class="auto-cursor-target">As shown in the following figure, there is only one endpoint seen in the organization associated with this user (manager). Hence, the analytics report displays information only about that endpoint to this user.</p>
        <p class="auto-cursor-target">
            <img src="../Resources/Images/Endpoint Associated with the Organization.png" style="border-left-style: solid;border-left-width: 1px;border-right-style: solid;border-right-width: 1px;border-top-style: solid;border-top-width: 1px;border-bottom-style: solid;border-bottom-width: 1px;width: 800px;height: 500px;" />
        </p>
        <p class="auto-cursor-target">Figure: Endpoint Associated with the Organization to which this Manager is associated&#160;</p>
        <h3>Configuring Organization Context-based ES Analytics Reports in IGA</h3>
        <p>To configure organization context in Analytics reports, you need to perform the following configurations&#160;while creating the analytics control from <strong style="text-align: left;">Create New Analytics Configuration</strong>page:</p>
        <ul>
            <li>Provide an SQL query with the keyword ORGANIZATIONCONTEXT with the customerkey in the <strong>Analytics Query</strong> field and&#160;</li>
            <li>Select the <strong>Context</strong> field as <strong>Organization</strong>.</li>
        </ul>
        <p>The following figure displays the configuration of the organization context for a sample ES Analytics Report.</p>
        <p>Here, the customerkey is associated with the keyword ORGANIZATIONCONTEXT in the SQL Query in&#160;<strong>Analytics Query</strong> field.</p>
        <p>
            <img src="../Resources/Images/Org_Context.png" style="border-left-style: solid;border-left-width: 0px;border-left-color: ;border-right-style: solid;border-right-width: 0px;border-right-color: ;border-top-style: solid;border-top-width: 0px;border-top-color: ;border-bottom-style: solid;border-bottom-width: 0px;border-bottom-color: ;width: 800px;height: 900px;" />
        </p>
        <p class="auto-cursor-target">Figure:&#160;Including the keyword ORGANIZATIONCONTEXT with customerkey in the Analytics Query and selecting the Context as Organization</p>
    </body>
</html>